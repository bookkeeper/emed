﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Helpers;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NewsController : Controller
    {
        private readonly EMedSovetEntities _db = new EMedSovetEntities();

        //
        // GET: /News/
        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(_db.News.ToList());
        }

        //
        // GET: /News/Details/5
        //[Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            News news = _db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        //
        // GET: /News/Create
        //[Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /News/Create
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(News news)
        {
            if (ModelState.IsValid)
            {
                news.Text = news.Text.ImgFileName();
                _db.News.Add(news);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(news);
        }

        //
        // GET: /News/Edit/5
        //[Authorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            News news = _db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        //
        // POST: /News/Edit/5
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(News news)
        {
            if (ModelState.IsValid)
            {
                news.Text = news.Text.ImgFileName();
                _db.Entry(news).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(news);
        }

        //
        // GET: /News/Delete/5
        //[Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            News news = _db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        //
        // POST: /News/Delete/5
        //[Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = _db.News.Find(id);
            _db.News.Remove(news);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        //
        // GET: /News/PartnerProgram
        [AllowAnonymous]
        public ActionResult PartnerProgram()
        {
            return View();
        }

        //
        // GET: /News/MedicalNews
        [AllowAnonymous]
        public ActionResult MedicalNews()
        {
            ViewBag.NewsList = _db.News.ToList();
            return View();
        }

        //
        // GET: /News/NewsDetail
        [AllowAnonymous]
        public ActionResult NewsDetail(int? id)
        {
            News news = _db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.News = news;
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult MedicalNewsLatest(string returnUrl)
        {
            return PartialView("_MedicalNewsLatest", _db.News.Take(2).ToList());
        }

    }
}