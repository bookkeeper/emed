﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using EMedSovet.Web.Chat;

namespace EMedSovet.Web.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private const string MAIN_ROOM_ID = "6CC8CE53-94D9-450C-A0DF-1CE47B3FE712";

        //
        // POST: /Chat/JoinRoom/
        [HttpPost]
        public ActionResult JoinRoom(string userId, string username, string avatarLink, string roomId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);
            if (room != null)
            {
                var res = room.JoinRoom(userId, username, avatarLink);
                return Json(res);
            }
            return Content(string.Empty);
        }


        //
        // POST: /Chat/CheckIfUserNameExists/
        [HttpPost]
        public ActionResult CheckIfUserNameExists(string userId, string roomId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);
            if (room != null)
            {
                var chatUser = room.GetUser(userId);
                return chatUser == null ? Json(false) : Json(chatUser.UserID == userId);
            }
            return Json(false);
        }

        //
        // POST: /Chat/CheckIfUserNameExists/
        [HttpPost]
        public ActionResult CheckIfRoomIdExists(string roomId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);
            if (room != null)
            {
                Json(true);
            }
            return Json(false);
        }


        //
        // POST: /Chat/UpdateUser/
        [HttpPost]
        public ActionResult UpdateUser(string roomId, string userId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);

            if (room != null)
            {
                var res = room.UpdateUser(userId);
                return Json(res);
            }
            return Content(string.Empty);
        }

        //
        // POST: /Chat/UpdateRoomMembers/
        [HttpPost]
        public ActionResult UpdateRoomMembers(string roomId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);
            if (room != null)
            {
                IEnumerable<string> users = room.GetRoomUsersNames();
                var res = new StringBuilder();

                foreach (var user in users)
                {
                    res.AppendFormat("{0}<br>", user);
                }
                return Json(res.ToString());
            }
            return Content(string.Empty);
        }

        //
        // POST: /Chat/SendMessage/
        [HttpPost]
        public ActionResult SendMessage(string msg, string roomId, string userId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);
            var res = string.Empty;
            if (room != null)
            {
                res = room.SendMessage(msg, userId);
            }
            return Json(res);
        }

        //
        // POST: /Chat/SendMessage/
        [HttpPost]
        public ActionResult LeaveRoom(string roomId, string userId)
        {
            var room = ChatEngine.GetRoom(string.IsNullOrEmpty(roomId) ? MAIN_ROOM_ID : roomId);
            if (room != null) room.LeaveRoom(userId);
            return Content(string.Empty);
        }


    }
}
