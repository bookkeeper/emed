﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
     [Authorize(Roles = "Admin")]
    public class ArticleController : Controller
    {
        private readonly EMedSovetEntities _db = new EMedSovetEntities();

        //
        // GET: /Article/

        public ActionResult Index()
        {
            return View(_db.Articles.ToList());
        }

        //
        // GET: /Article/Details/5

        public ActionResult Details(int id = 0)
        {
            Article article = _db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        //
        // GET: /Article/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Article/Create

        [HttpPost]
        public ActionResult Create(Article article)
        {
            if (ModelState.IsValid)
            {
                _db.Articles.Add(article);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(article);
        }

        //
        // GET: /Article/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Article article = _db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        //
        // POST: /Article/Edit/5

        [HttpPost]
        public ActionResult Edit(Article article)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(article).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(article);
        }

        //
        // GET: /Article/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Article article = _db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        //
        // POST: /Article/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Article article = _db.Articles.Find(id);
            _db.Articles.Remove(article);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        //
        // GET: /News/MedicalArticles
        [AllowAnonymous]
        public ActionResult MedicalArticles()
        {
            ViewBag.ArticlesList = _db.Articles.ToList();
            return View();
        }

        //
        // GET: /News/ArticleDetail
        [AllowAnonymous]
        public ActionResult ArticleDetail(int? id)
        {
            var article = _db.Articles.Find(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            ViewBag.Article = article;
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult MedicalArticlesLatest(string returnUrl)
        {
            return PartialView("_MedicalArticlesLatest", _db.Articles.Take(2).ToList());
        }
    }
}