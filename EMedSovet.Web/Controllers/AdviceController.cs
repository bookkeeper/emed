﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
    
    public class AdviceController : BaseController
    {

        //
        // GET: /Advice/
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var advices = _db.Advices.Include(a => a.Doctor).Include(a => a.Patient).Include(a => a.AdviceStatus);
            return View(advices.ToList());
        }

        //
        // GET: /Advice/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            Advice advice = _db.Advices.Find(id);
            if (advice == null)
            {
                return HttpNotFound();
            }
            return View(advice);
        }

        //
        // GET: /Advice/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.DoctorId = new SelectList(_db.Doctors, "DoctorId", "ConsultationArea");
            ViewBag.PatientId = new SelectList(_db.Patients, "PatientId", "Foto");
            ViewBag.AdviceStatusId = new SelectList(_db.AdviceStatuses, "AdviceStatusId", "Name");
            return View();
        }

        //
        // POST: /Advice/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(Advice advice)
        {
            if (ModelState.IsValid)
            {
                _db.Advices.Add(advice);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DoctorId = new SelectList(_db.Doctors, "DoctorId", "ConsultationArea", advice.DoctorId);
            ViewBag.PatientId = new SelectList(_db.Patients, "PatientId", "Foto", advice.PatientId);
            ViewBag.AdviceStatusId = new SelectList(_db.AdviceStatuses, "AdviceStatusId", "Name", advice.AdviceStatusId);
            return View(advice);
        }

        //
        // GET: /Advice/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            Advice advice = _db.Advices.Find(id);
            if (advice == null)
            {
                return HttpNotFound();
            }
            ViewBag.DoctorId = new SelectList(_db.Doctors, "DoctorId", "ConsultationArea", advice.DoctorId);
            ViewBag.PatientId = new SelectList(_db.Patients, "PatientId", "Foto", advice.PatientId);
            ViewBag.AdviceStatusId = new SelectList(_db.AdviceStatuses, "AdviceStatusId", "Name", advice.AdviceStatusId);
            return View(advice);
        }

        //
        // POST: /Advice/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Advice advice)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(advice).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DoctorId = new SelectList(_db.Doctors, "DoctorId", "ConsultationArea", advice.DoctorId);
            ViewBag.PatientId = new SelectList(_db.Patients, "PatientId", "Foto", advice.PatientId);
            ViewBag.AdviceStatusId = new SelectList(_db.AdviceStatuses, "AdviceStatusId", "Name", advice.AdviceStatusId);
            return View(advice);
        }

        //
        // GET: /Advice/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            Advice advice = _db.Advices.Find(id);
            if (advice == null)
            {
                return HttpNotFound();
            }
            return View(advice);
        }

        //
        // POST: /Advice/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Advice advice = _db.Advices.Find(id);
            _db.Advices.Remove(advice);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Advice/ChangeAdviceStatus/5?adviceStatusId=2
        [Authorize(Roles = "Admin, Doctor")]
        public ActionResult ChangeAdviceStatus(int? id, int? adviceStatusId)
        {
            var advice = _db.Advices.Find(id);
            if (advice == null || !adviceStatusId.HasValue)
            {
                return HttpNotFound();
            }
            advice.AdviceStatusId = adviceStatusId.Value;
            advice.CompletedAt = adviceStatusId == 4 ? (DateTime?) DateTime.Now : null;
            _db.SaveChanges();
            return RedirectToAction("PersonalArea", "Doctor");
            //return View(advice);
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}