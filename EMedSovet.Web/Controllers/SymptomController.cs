﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
    public class SymptomController : Controller
    {
        private readonly EMedSovetEntities _db = new EMedSovetEntities();

        //
        // GET: /Symptom/

        
        public ActionResult ChooseSymptoms()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetSymptoms(string bodyPart, string sex)
        {
            return Json(_db.Symptoms.ToList());
        }

        public ActionResult Index()
        {
            return View(_db.Symptoms.ToList());
        }

        //
        // GET: /Symptom/Details/5

        public ActionResult Details(int id = 0)
        {
            Symptom symptom = _db.Symptoms.Find(id);
            if (symptom == null)
            {
                return HttpNotFound();
            }
            return View(symptom);
        }

        //
        // GET: /Symptom/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Symptom/Create

        [HttpPost]
        public ActionResult Create(Symptom symptom)
        {
            if (ModelState.IsValid)
            {
                _db.Symptoms.Add(symptom);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(symptom);
        }

        //
        // GET: /Symptom/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Symptom symptom = _db.Symptoms.Find(id);
            if (symptom == null)
            {
                return HttpNotFound();
            }
            return View(symptom);
        }

        //
        // POST: /Symptom/Edit/5

        [HttpPost]
        public ActionResult Edit(Symptom symptom)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(symptom).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(symptom);
        }

        //
        // GET: /Symptom/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Symptom symptom = _db.Symptoms.Find(id);
            if (symptom == null)
            {
                return HttpNotFound();
            }
            return View(symptom);
        }

        //
        // POST: /Symptom/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Symptom symptom = _db.Symptoms.Find(id);
            _db.Symptoms.Remove(symptom);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}