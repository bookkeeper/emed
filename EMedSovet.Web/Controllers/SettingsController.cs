﻿using System.Web.Mvc;
using EMedSovet.Web.Filters;

namespace EMedSovet.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [InitializeSimpleMembership]
    public class SettingsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UploadFiles()
        {
            return View();
        }
    }
}
