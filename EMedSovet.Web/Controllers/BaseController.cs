﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Helpers;
using EMedSovet.Web.Models;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly char[] _specialtiesOptGroup = new[]
            {
                'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И',
                'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
                'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ', 'Э', 'Ю', 'Я'
            };

        protected readonly EMedSovetEntities _db = new EMedSovetEntities();

        protected const int MAX_ICON_SIZE = 1048576;
        protected const string FOTO_INPUT_NAME = "FotoUpload";
        protected const string DOC_SCAN_INPUT_NAME = "DocScanUpload";
        #region Overrides

        /// <summary>
        /// Fill ViewBag with often used data
        /// </summary>
        /// <param name="filterContext"></param>
        /// <remarks>
        /// added by ziro
        /// </remarks>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            string cultureName = null;
            if (filterContext.Result is ViewResultBase)
            {
                // fill ViewBag with often used data here
                //var currentUser = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
                //if (currentUser != null)
                //{
                //    var doctor = _db.Doctors.FirstOrDefault(d => d.UserId == currentUser.UserId);
                //    if (doctor != null)
                //    {
                //        ViewBag.CurrentDoctor = doctor;
                //    }

                //}
                //if (filterContext.HttpContext.Request.Cookies["culture"] != null)
                //{
                //    var culturecookie = filterContext.HttpContext.Request.Cookies["culture"];
                //    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(culturecookie.Value);
                //}

                
                // Attempt to read the culture cookie from Request
                HttpCookie cultureCookie = Request.Cookies["culture"];
                if (cultureCookie != null)
                    cultureName = cultureCookie.Value;
                else
                    cultureName = Request.UserLanguages[0]; // obtain it from HTTP header AcceptLanguages

                // Validate culture name
                cultureName = CultureHelper.GetValidCulture(cultureName); // This is safe



                // Modify current thread's culture            
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);

            }

            // Is it View ?
            ViewResultBase view = filterContext.Result as ViewResultBase;
            if (view == null || filterContext.IsChildAction) // if not exit
                return;

            cultureName = Thread.CurrentThread.CurrentCulture.Name; // e.g. "en-US" // filterContext.HttpContext.Request.UserLanguages[0]; // needs validation return "en-us" as default            

            // Is it default culture? exit
            if (cultureName == CultureHelper.GetDefaultCulture())
                return;


            // Are views implemented separately for this culture?  if not exit
            bool viewImplemented = CultureHelper.IsViewSeparate(cultureName);
            if (viewImplemented == false)
                return;

            string viewName = view.ViewName;

            int i = 0;

            if (string.IsNullOrEmpty(viewName))
                viewName = filterContext.RouteData.Values["action"] + "." + cultureName; // Index.en-US
            else if ((i = viewName.IndexOf('.')) > 0)
            {
                // contains . like "Index.cshtml"                
                viewName = viewName.Substring(0, i + 1) + cultureName + viewName.Substring(i);
            }
            else
                viewName += "." + cultureName; // e.g. "Index" ==> "Index.en-Us"
            
            var result = ViewEngines.Engines.FindView(filterContext, viewName, null);
            if (result.View != null)
            {
                view.ViewName = viewName;
            }

            filterContext.Controller.ViewBag._culture = "." + cultureName;


            base.OnActionExecuted(filterContext);
        }

        //protected override void ExecuteCore()
        //{
        //    string cultureName = null;
        //    // Attempt to read the culture cookie from Request
        //    HttpCookie cultureCookie = Request.Cookies["culture"];
        //    if (cultureCookie != null)
        //        cultureName = cultureCookie.Value;
        //    else
        //        cultureName = Request.UserLanguages[0]; // obtain it from HTTP header AcceptLanguages

        //    // Validate culture name
        //    cultureName = CultureHelper.GetValidCulture(cultureName); // This is safe



        //    // Modify current thread's culture            
        //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
        //    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);


        //    base.ExecuteCore();
        //}

        #endregion


        protected void SaveFile(string inputName, string path)
        {
            foreach (string inputTagName in Request.Files)
            {
                var file = Request.Files[inputTagName];
                if (file != null && !string.IsNullOrWhiteSpace(file.FileName) && inputTagName == inputName)
                    if (file.ContentLength > 0)
                    {
                        //return ImageToByteArray(ImageFromHttpPostedFileBase(file), inputname);
                        var filePath = Path.Combine(string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, path), Path.GetFileName(file.FileName) ?? "test.tmp");
                        file.SaveAs(filePath);
                    }
            }
        }

        protected bool IsIconSizeExceeded(string inputname)
        {
            foreach (string inputTagName in Request.Files)
            {
                var file = Request.Files[inputTagName];
                if (file != null && inputTagName == inputname)
                    if (file.ContentLength <= MAX_ICON_SIZE)
                    {
                        return false;
                    }
            }
            return true;
        }

        protected string GetUploadedFileName(string inputname)
        {
            foreach (string inputTagName in Request.Files)
            {
                var file = Request.Files[inputTagName];
                if (file != null && inputTagName == inputname)
                    if (file.ContentLength > 0)
                    {
                        //var fileinfo = new FileInfo(Regex.Replace(file.FileName, " ", "_"));
                        //return fileinfo.Name;
                        return file.FileName;
                    }
            }
            return string.Empty;

        }

        protected List<GroupDropListItem> GetGroupList(IQueryable<Specialty> specialties)
        {
            var groupList = new List<GroupDropListItem>();
            foreach (var optGroup in _specialtiesOptGroup)
            {
                var specialtiesEntities = new List<Specialty>();
                foreach (var specialty in specialties)
                {
                    if(specialty.Name.ToLower().StartsWith(optGroup.ToString(CultureInfo.CurrentCulture).ToLower()))
                        specialtiesEntities.Add(specialty);
                }

                var optionItems =
                    (from s in specialtiesEntities
                     select new OptionItem {Text = s.Name, Value = s.SpecialtyId.ToString(CultureInfo.InvariantCulture)}).ToList();
                if (optionItems.Any())
                    groupList.Add(new GroupDropListItem
                        {
                            Name = optGroup.ToString(CultureInfo.CurrentCulture),
                            Items = optionItems
                        });
            }

            return groupList;

        }

        protected List<GroupDropListItem> GetGroupList(IQueryable<Disease> diseases)
        {
            var groupList = new List<GroupDropListItem>();
            foreach (var optGroup in _specialtiesOptGroup)
            {
                var diseasesEntities = new List<Disease>();
                foreach (var disease in diseases)
                {
                    if (disease.Name.ToLower().StartsWith(optGroup.ToString(CultureInfo.CurrentCulture).ToLower()))
                        diseasesEntities.Add(disease);
                }

                var optionItems =
                    (from s in diseasesEntities
                     select new OptionItem { Text = s.Name, Value = s.DiseaseId.ToString(CultureInfo.InvariantCulture) }).ToList();
                if (optionItems.Any())
                    groupList.Add(new GroupDropListItem
                    {
                        Name = optGroup.ToString(CultureInfo.CurrentCulture),
                        Items = optionItems
                    });
            }

            return groupList;

        }

        protected List<GroupDropListItem> GetGroupList(IQueryable<Doctor> doctors)
        {
            var groupList = new List<GroupDropListItem>();
            foreach (var optGroup in _specialtiesOptGroup)
            {
                var doctorEntities = new List<Doctor>();
                foreach (var doctor in doctors)
                {
                    if (doctor.UserProfile.Surname.ToLower().StartsWith(optGroup.ToString(CultureInfo.CurrentCulture).ToLower()))
                        doctorEntities.Add(doctor);
                }

                var optionItems =
                    (from d in doctorEntities
                     select new OptionItem { Text = string.Format("{0} {1}", d.UserProfile.Surname, d.UserProfile.FirstName), Value = d.DoctorId.ToString(CultureInfo.InvariantCulture) }).OrderBy(i => i.Text).ToList();
                if (optionItems.Any())
                    groupList.Add(new GroupDropListItem
                    {
                        Name = optGroup.ToString(CultureInfo.CurrentCulture),
                        Items = optionItems
                    });

            }

            return groupList;

        }

        protected SelectList GetConfirmationList(bool? isSelected)
        {
            return new SelectList(new List<object>
                {
                    new {Name = Resources.Yes, Value="true"},
                    new {Name = Resources.No, Value="false"}
                }, "Value", "Name", isSelected.HasValue && isSelected.Value ? "true" : "false");
        }
    }
}
