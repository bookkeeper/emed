﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Helpers;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{

    public class MessageController : BaseController
    {
        private EMedSovetEntities db = new EMedSovetEntities();

        //
        // GET: /Message/
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var messages = db.Messages.Include(m => m.UserProfileTo);
            return View(messages.ToList());
        }

        //
        // GET: /Message/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        //
        // GET: /Message/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.To = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Message/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(Message message)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.To = new SelectList(db.UserProfiles, "UserId", "UserName", message.To);
            return View(message);
        }

        //
        // GET: /Message/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            ViewBag.To = new SelectList(db.UserProfiles, "UserId", "UserName", message.To);
            return View(message);
        }

        //
        // POST: /Message/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Message message)
        {
            if (ModelState.IsValid)
            {
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.To = new SelectList(db.UserProfiles, "UserId", "UserName", message.To);
            return View(message);
        }

        //
        // GET: /Message/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        //
        // POST: /Message/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.Messages.Find(id);
            db.Messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        //
        // GET: /Message/Delete/5
        [Authorize(Roles = "Admin, Doctor, Patient")]
        public ActionResult MyMessages()
        {
            var messages = new List<Message>();
            if (User.Identity.IsAuthenticated)
            {
                var user = _db.UserProfiles
                              .Include(u => u.MessagesTo.Select(m => m.UserProfileTo))
                              .Include(u => u.MessagesTo.Select(m => m.UserProfileFrom))
                              .Include(u => u.MessagesTo.Select(m => m.MessagesAnswer))
                              .Include(u => u.MessagesFrom.Select(m => m.UserProfileTo))
                              .Include(u => u.MessagesFrom.Select(m => m.UserProfileFrom))
                              .Include(u => u.MessagesFrom.Select(m => m.MessagesAnswer))
                              .FirstOrDefault(u => u.UserName.ToLower() == User.Identity.Name.ToLower());
                if (user != null)
                {
                    //messages = user.MessagesTo.Where(m => !m.AnswerId.HasValue).ToList();
                    messages = user.MessagesTo.Where(m => !m.AnswerId.HasValue).ToList();
                    messages.AddRange(user.MessagesFrom.Where(m => !m.AnswerId.HasValue).ToList());
                    ViewBag.UserId = user.UserId;
                }
            }
            return View(messages);
        }


        //
        // POST: /Message/Edit/5
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendMessageToAllAdminUsers(Contact contact)
        {
            var users = _db.UserProfiles
                .Include(u => u.Roles)
                .Include(u => u.MessagesTo);

            var adminUsers = (from userProfile in users from role in userProfile.Roles where role.RoleName == "Admin" select userProfile).ToList();
            var body = new StringBuilder();
            body.AppendFormat("Message from: {0}<br/>", contact.Name);
            body.AppendFormat("{0}", contact.Text);
            foreach (var userProfile in adminUsers)
            {
                if (string.IsNullOrWhiteSpace(userProfile.Email)) continue;
                MailHelper.SendMail(contact.Email, userProfile.Email, contact.Subject, body.ToString());
                var message = new Message
                    {
                        Subject = contact.Subject,
                        To = userProfile.UserId,
                        Email = contact.Email,
                        Text = body.ToString(),
                        CreatedAt = DateTime.Now
                    };
                if (contact.From != null) message.From = contact.From.Value;
                if (message.From.HasValue && message.To.HasValue && message.From.Value == message.To.Value) continue;
                userProfile.MessagesTo.Add(message);
            }
            var infoMail = ConfigurationManager.AppSettings["InfoEmail"];
            MailHelper.SendMail(contact.Email, infoMail, contact.Subject, body.ToString());
            _db.SaveChanges();
            if (Request.IsAjaxRequest())
            {
                return Json("mail sent");
            }
            return RedirectToAction("Login", "Account");
        }


        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult AnswerForm()
        {
            //var messageAnswer = new MessageAnswer();
            return PartialView("_AnswerForm", new MessageAnswer());
        }


        //
        // GET: /Message/Delete/5
        [Authorize(Roles = "Admin, Doctor, Patient")]
        public ActionResult AnswerMessage(MessageAnswer messageAnswer)
        {
            var message = _db.Messages
                .Include(m => m.MessagesAnswer)
                .FirstOrDefault(m => m.MessageId == messageAnswer.MessageId);
            UserProfile user = null;
            if (User.Identity.IsAuthenticated)
            {
                user = _db.UserProfiles

                              .FirstOrDefault(u => u.UserName.ToLower() == User.Identity.Name.ToLower());
            }
            if (message != null)
            {
                var answer = new Message
                    {
                        Subject = string.Format("Re: {0}", message.Subject),
                        Text = messageAnswer.Text,
                        CreatedAt = DateTime.Now
                    };
                if (user != null) answer.From = user.UserId;
                if (user != null) answer.Email = user.Email;
                if (message.From != null) answer.To = message.From.Value;
                _db.Messages.Add(answer);
                message.MessagesAnswer.Add(answer);
                _db.SaveChanges();
                MailHelper.SendMail(answer.Email, message.Email, answer.Subject, answer.Text);
                var infoMail = ConfigurationManager.AppSettings["InfoEmail"];
                MailHelper.SendMail(answer.Email, infoMail, answer.Subject, answer.Text);
            }


            if (Request.IsAjaxRequest())
            {
                return Json("answer sent");
            }
            return RedirectToAction("MyMessages");
        }
    }
}