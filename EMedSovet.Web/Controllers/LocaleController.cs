﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Threading;

namespace EMedSovet.Web.Controllers
{
    public class LocaleController : Controller
    {
        [HttpPost]
        public ActionResult CurrentCulture()
        {
            return Json(Thread.CurrentThread.CurrentUICulture.ToString());
        }

        [HttpPost]
        public ActionResult ChangeCulture(string culture)
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(culture);
            SetLanguageCookies(culture);
            return Json("success");
        }

        private void SetLanguageCookies(string culture)
        {
            var cookieuserid = new HttpCookie("culture", culture); //{Expires = DateTime.Now.AddHours(1)};
            Response.Cookies.Add(cookieuserid);
        }


    }
}
