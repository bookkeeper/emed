﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using EMedSovet.Web.Helpers;
using EMedSovet.Web.Models.Dto;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using EMedSovet.Web.Filters;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : BaseController
    {
        //
        // GET: /Account/Login
        private const int USERNAME_LENGTH = 6;
        private const int PASSWORD_LENGTH = 6;

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var specialties = _db.Specialties.OrderBy(s => s.Name);
            var diseases = _db.Diseases.OrderBy(d => d.Name);
            var doctors = _db.Doctors.Include("UserProfile");
            ViewBag.Specialties = GetGroupList(specialties);
            ViewBag.Diseases = GetGroupList(diseases);
            ViewBag.Doctors = GetGroupList(doctors);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                //if (!CheckIsRoleExists(model.UserName))
                //{
                //    return RedirectToAction("SetUserRole");
                //}
                //if (IsDoctor(model.UserName) && !CheckIsDoctorOrPatientProfileFilled(model.UserName))
                //{
                //    return RedirectToAction("FillDoctorsCard", "Doctor");
                //}
                return RedirectToInfoByUsername(model.UserName, returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View();
            //return RedirectToAction("Index","Home");
        }

        private bool IsPatient(string username)
        {
            var user = _db.UserProfiles.Include("Roles").FirstOrDefault(u => u.UserName.ToLower() == username.ToLower());

            if (user == null) return false;
            var role = user.Roles.FirstOrDefault();
            if (role == null) return false;
            return role.RoleName == "Patient";

        }
        private bool IsDoctor(string username)
        {
            var user = _db.UserProfiles.Include("Roles").FirstOrDefault(u => u.UserName.ToLower() == username.ToLower());

            if (user == null) return false;
            var role = user.Roles.FirstOrDefault();
            if (role == null) return false;
            return role.RoleName == "Doctor";

        }

        private bool CheckIsRoleExists(string username)
        {
            var user = _db.UserProfiles.Include("Roles").FirstOrDefault(u => u.UserName.ToLower() == username.ToLower());
            //var membership = _db.Memberships.Include("Roles").FirstOrDefault(m => m.UserId == user.UserId);
            if (user != null)
            {
                var roles = user.Roles;
                // Check if role for current user already exists
                if (roles.Any()) return true;

            }
            return false;
        }

        private bool CheckIsDoctorOrPatientProfileFilled(string username)
        {
            var user = _db.UserProfiles.Include("Doctor").FirstOrDefault(u => u.UserName.ToLower() == username.ToLower());

            if (user == null) return false;
            var doctors = user.Doctor;
            if (doctors != null)
            {
                if (doctors.Any()) return true;
            }
            return false;
        }

        [Authorize]
        public ActionResult SetUserRole()
        {

            return View();
        }

        [Authorize]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SetUserRole(string role)
        {
            var user = _db.UserProfiles.Include("Roles").FirstOrDefault(u => u.UserName.ToLower() == User.Identity.Name.ToLower());
            //var membership = _db.Memberships.Include("Roles").FirstOrDefault(m => m.UserId == user.UserId);
            Role roles;
            if (user != null && role == "doctor" && user.Roles.Count == 0)
            {
                roles = _db.Roles.FirstOrDefault(r => r.RoleName == "Doctor");
                user.Roles.Add(roles);
            }
            if (user != null && role == "patient" && user.Roles.Count == 0)
            {
                roles = _db.Roles.FirstOrDefault(r => r.RoleName == "Patient");
                user.Roles.Add(roles);
            }
            _db.SaveChanges();

            if (user != null && role == "doctor")
            {
                if (user.Doctor.Count == 0)
                {
                    return RedirectToAction("FillDoctorsCard", "Doctor");
                }
            }
            if (user != null && role == "patient")
            {
                return RedirectToAction("PersonalCard", "Patient");
            }
            //patient

            return RedirectToAction("Login", "Account");
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Login");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    RegisterUser(model);

                    if (model.Role == "doctor")
                    {
                        return RedirectToAction("Info", "Doctor");
                    }
                    if (model.Role == "patient")
                    {
                        return RedirectToAction("Info", "Patient");
                    }
                    //if (!CheckIsRoleExists(model.UserName))
                    //{
                    //    return RedirectToAction("SetUserRole");
                    //}
                    //if (!CheckIsDoctorOrPatientProfileFilled(model.UserName))
                    //{
                    //    return RedirectToAction("FillDoctorsCard", "Doctor");
                    //}
                    //return RedirectToAction("Login");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            //return View(model);
            return RedirectToAction("Login");
        }

        private void RegisterUser(RegisterModel model)
        {
            WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
            WebSecurity.Login(model.UserName, model.Password);
            // Set email for new user
            //var user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
            var user = _db.UserProfiles.Include("Roles").Include("Doctor").Include("Patient").FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
            // Check if user already exists
            if (user == null) return;
            //Insert name into the profile table
            user.Email = model.Email;
            user.IsPersonalDataConfirmed = model.IsPersonalDataConfirmed;
            user.GenderId = 1;
            user.Surname = string.Empty;
            user.FirstName = string.Empty;
            user.SecondName = string.Empty;

            SetRoles(model.Role, user);

            _db.SaveChanges();

        }

        private void SetRoles(string role, UserProfile user)
        {
            Role roles;
            if (role == "doctor" && user.Roles.Count == 0)
            {
                roles = _db.Roles.FirstOrDefault(r => r.RoleName == "Doctor");
                user.Roles.Add(roles);
                var doctor = new Doctor { UserId = user.UserId, IsConfirmed = false, IsActive = false};
                user.Doctor.Add(doctor);
            }
            if (role == "patient" && user.Roles.Count == 0)
            {
                roles = _db.Roles.FirstOrDefault(r => r.RoleName == "Patient");
                user.Roles.Add(roles);
                var patient = new Patient { UserId = user.UserId };
                user.Patient.Add(patient);
            }
        }


        //
        // GET: /Account/RegisterScan

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult RegisterScan(RegisterModel model)
        {
            string username;
            do
            {
                username = GetRandomString(USERNAME_LENGTH);
            } while (IsUserNameUnique(username));

            var password = GetRandomString(PASSWORD_LENGTH);
            model.UserName = username;
            model.Password = password;
            model.ConfirmPassword = password;
            try
            {
                RegisterUser(model);
                //Set roles
                var user = _db.UserProfiles.Include("Roles").Include("Doctor").FirstOrDefault(u => u.UserName.ToLower() == username.ToLower());
                if (user != null && user.Roles.Count == 0)
                {
                    var roles = _db.Roles.FirstOrDefault(r => r.RoleName == "Doctor");
                    user.Roles.Add(roles);
                    //Create doctor profile
                    var doctor = new Doctor
                        {
                            UserId = user.UserId,
                            //SpecialtyId = 1,
                            IsConfirmed = false
                        };

                    if (SaveDoctorUploads(ModelState, doctor))
                    {
                        user.Doctor.Add(doctor);
                        _db.SaveChanges();
                        //SendMail(model.Email); 
                    }
                }
            }
            catch (MembershipCreateUserException e)
            {

                ModelState.AddModelError(string.Empty, ErrorCodeToString(e.StatusCode));
            }
            WebSecurity.Logout();
            return RedirectToAction("RegisterScanConfirmation");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult RegisterScanConfirmation()
        {
            return View();
        }

        private bool SaveDoctorUploads(ModelStateDictionary modelState, Doctor doctor)
        {
            if (IsIconSizeExceeded(DOC_SCAN_INPUT_NAME))
            {
                ModelState.AddModelError("Common", "Icon size too large, cannot be more than 1MB");
                {
                    return false;
                }
            }
            SaveFile(DOC_SCAN_INPUT_NAME, "Uploads\\Doctors\\Docs");
            doctor.DocScan = GetUploadedFileName(DOC_SCAN_INPUT_NAME);
            return true;
        }

        private static string GetRandomString(int length)
        {
            const string allowedChars = "abcdefghjklmnopqrstuvwxyz0123456789";
            var rd = new Random();
            var chars = new char[length];
            for (var i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        private bool IsUserNameUnique(string username)
        {
            return Enumerable.Any(_db.UserProfiles, userProfile => userProfile.UserName.ToLower() == username.ToLower());
        }


        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToInfoByUsername(result.UserName, returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToInfoByUsername(result.UserName, returnUrl);


            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        private ActionResult RedirectToInfoByUsername(string username, string returnUrl)
        {
            if (IsAdmin(username))
            {
                return RedirectToAction("Login", "Account");
            }
            if (IsDoctor(username))
            {
                return RedirectToAction("PersonalArea", "Doctor");
            }
            if (IsPatient(username))
            {
                return RedirectToAction("PersonalArea", "Patient");
            }
            return RedirectToLocal(returnUrl);
        }

        private bool IsAdmin(string username)
        {
            var user = _db.UserProfiles.Include("Roles").FirstOrDefault(u => u.UserName.ToLower() == username.ToLower());

            if (user == null) return false;
            var role = user.Roles.FirstOrDefault();
            if (role == null) return false;
            return role.RoleName == "Admin";
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                //// Insert a new user into the database
                //using (UsersContext db = new UsersContext())
                //{
                //    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                //    // Check if user already exists
                //    if (user == null)
                //    {
                //        // Insert name into the profile table
                //        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                //        db.SaveChanges();

                //        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                //        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                //        return RedirectToLocal(returnUrl);
                //    }
                //    else
                //    {
                //        ModelState.AddModelError("UserName", "User name already exists. Please enter a different user name.");
                //    }
                //}


                // Insert a new user into the database
                //var user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                var user = _db.UserProfiles.Include("Roles").Include("Doctor").Include("Patient").FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                // Check if user already exists
                if (user == null)
                {
                    // Insert name into the profile table
                    //Role roles = null;
                    //if (model.Role == "doctor")
                    //{
                    //    roles = db.Roles.FirstOrDefault(r => r.RoleName == "Doctor");
                    //}
                    //if (model.Role == "patient")
                    //{
                    //    roles = db.Roles.FirstOrDefault(r => r.RoleName == "Patient");
                    //}

                    var userProfile = new UserProfile
                        {
                            UserName = model.UserName,
                            IsPersonalDataConfirmed = model.IsPersonalDataConfirmed,
                            GenderId = 1,
                            Surname = string.Empty,
                            FirstName = string.Empty,
                            SecondName = string.Empty
                        };

                    SetRoles(model.Role, userProfile);
                    //if (roles != null)
                    //{
                    //    userProfile.Roles.Add(roles);
                    //}

                    _db.UserProfiles.Add(userProfile);
                    _db.SaveChanges();

                    OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                    OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                    return RedirectToInfo(model.Role, returnUrl);
                }
                else
                {
                    ModelState.AddModelError("UserName", "User name already exists. Please enter a different user name.");
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        private ActionResult RedirectToInfo(string role, string returnUrl)
        {
            if (role == "doctor")
            {
                {
                    return RedirectToAction("Info", "Doctor");
                }
            }
            if (role == "patient")
            {
                {
                    return RedirectToAction("Info", "Patient");
                }
            }
            return RedirectToLocal(returnUrl);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalSocialsPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult LoginForm()
        {
            return PartialView("_LoginForm");
        }


        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ContactForm()
        {
            var contact = new Contact();
            if (User.Identity.IsAuthenticated)
            {
                var user = _db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == User.Identity.Name.ToLower());
                if (user != null)
                {
                    contact.Name = user.GetFirstLastName();
                    contact.Email = user.Email;
                    contact.From = user.UserId;
                }
            }
            return PartialView("_ContactForm", contact);
        }
    }
}
