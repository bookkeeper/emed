﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EMedSovet.Web.Models;
using EMedSovet.Web.Models.Dto;
using EMedSovet.Web.Upload;

namespace EMedSovet.Web.Controllers
{
    public class PatientController : BaseController
    {
        //private readonly EMedSovetEntities _db = new EMedSovetEntities();

        //
        // GET: /Patient/

        public ActionResult Index()
        {
            var patients = _db.Patients
                .Include(p => p.UserProfile)
                .Include(p => p.UserProfile.Gender);
            return View(patients.ToList());
        }

        //
        // GET: /Patient/Info
        [Authorize(Roles = "Admin, Patient")]
        public ActionResult Info()
        {
            return View();
        }

        //
        // GET: /Patient/PersonalArea
        [Authorize(Roles = "Admin, Patient")]
        public ActionResult PersonalArea()
        {
            var currentUser = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
            Patient patient = null;
            if (currentUser != null)
            {
                patient = _db.Patients
                    .Include(p => p.UserProfile)
                    .Include(p => p.UserProfile.Gender)
                    .Include(p => p.Advices.Select(a => a.AdviceStatus))
                    .Include(p => p.Advices.Select(a => a.Doctor).Select(d => d.UserProfile))
                    .FirstOrDefault(p => p.UserId == currentUser.UserId);
                if (patient == null)
                {
                    return HttpNotFound();
                }
            }
            return View(patient);
        }

        //
        // GET: /Patient/FillPatientCard
        [Authorize(Roles = "Admin, Patient")]
        public ActionResult FillPatientCard()
        {
            return View();
        }

        //
        // GET: /Patient/Details/5

        public ActionResult Details(int id = 0)
        {
            //var patient = _db.Patients.Find(id);
            var patient = _db.Patients
                .Include(p => p.UserProfile)
                .FirstOrDefault(u => u.PatientId == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(patient);
        }

        //
        // GET: /Patient/Create

        public ActionResult Create()
        {
            SetDropDownsOnCreate();
            //ViewBag.UserId = new SelectList(_db.UserProfiles, "UserId", "UserName");
            ViewBag.Gender = SetGenders(null);
            ViewBag.ConfirmationList = GetConfirmationList(false);
            return View();
        }

        private void SetDropDownsOnCreate()
        {
            ViewBag.SymptomsList = new MultiSelectList(_db.Symptoms, "SymptomId", "Name");
            var userIds = _db.Patients.ToList().Select(p => p.UserId).ToList();
            var users = new List<UserProfile>();
            foreach (var userProfile in _db.UserProfiles.Include("Roles").Where(userProfile => !userIds.Contains(userProfile.UserId)))
            {
                users.AddRange(from role in userProfile.Roles where role.RoleName == "Patient" select userProfile);
            }

            ViewBag.UserId = new SelectList(users, "UserId", "UserName");
        }

        //
        // POST: /Patient/Create

        [HttpPost]
        public ActionResult Create(Patient patient, UserProfileDto userProfileDto)
        {
            //var symptoms = collection["Symptoms"];
            //var symptomsList = GetSymptoms(symptoms);
            //patient.Symptom.Clear();
            //foreach (var symptom in symptomsList)
            //{
            //    patient.Symptom.Add(symptom);
            //}
            var user = _db.UserProfiles.FirstOrDefault(u => u.UserId == patient.UserId);
            if (user != null)
            {
                user.FirstName = userProfileDto.FirstName;
                user.SecondName = userProfileDto.SecondName;
                user.Surname = userProfileDto.Surname;
                user.Age = userProfileDto.Age;
                user.GenderId = userProfileDto.GenderId;
            }
            if (ModelState.IsValid)
            {
                _db.Patients.Add(patient);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            SetDropDownsOnCreate();
            ViewBag.Gender = SetGenders(userProfileDto.GenderId);
            ViewBag.ConfirmationList = GetConfirmationList(patient.IsActive);
            //ViewBag.UserId = new SelectList(_db.UserProfiles, "UserId", "UserName", patient.UserId);
            return View(patient);
        }

        private IEnumerable<Symptom> GetSymptoms(string symptoms)
        {
            var symptomsList = new List<Symptom>();
            if (string.IsNullOrWhiteSpace(symptoms)) return symptomsList;
            foreach (var symptom in symptoms.Split(new[] { ',' }))
            {
                var symptomId = Convert.ToInt32(symptom);
                var symptomEntity = _db.Symptoms.FirstOrDefault(s => s.SymptomId == symptomId);
                if (symptomEntity != null)
                {
                    symptomsList.Add(symptomEntity);
                }
            }
            return symptomsList;
        }

        //
        // GET: /Patient/Edit/5

        public ActionResult Edit(int id = 0)
        {
            //Patient patient = _db.Patients.Find(id);
            var patient = _db.Patients
                .Include(p => p.Symptom)
                .Include(u => u.UserProfile)
                .Include(u => u.UserProfile.Gender)
                .FirstOrDefault(p => p.PatientId == id);
            int? genderId = null;
            if (patient == null)
            {
                return HttpNotFound();
            }
            genderId = patient.UserProfile.Gender != null ? patient.UserProfile.Gender.GenderId : (int?)null;
            SetDropDownsOnEdit(patient);
            ViewBag.Gender = SetGenders(genderId);
            //ViewBag.UserId = new SelectList(_db.UserProfiles, "UserId", "UserName", patient.UserId);
            return View(patient);
        }

        //
        // POST: /Patient/Edit/5

        [HttpPost]
        public ActionResult Edit(Patient patient, UserProfileDto userProfileDto)
        {

            //var patientEntity = _db.Patients.Include("Symptom").FirstOrDefault(p => p.PatientId == patient.PatientId);
            var patientEntity = _db.Patients
                .Include(p => p.UserProfile)
                .FirstOrDefault(p => p.PatientId == patient.PatientId);
            if (ModelState.IsValid)
            {
                if (patientEntity == null) return RedirectToAction("Index");
                //var symptoms = collection["Symptoms"];
                //var symptomsList = GetSymptoms(symptoms);
                //patientEntity.Symptom.Clear();
                //foreach (var symptom in symptomsList)
                //{
                //    patientEntity.Symptom.Add(symptom);
                //}
                //_db.Entry(patient).State = EntityState.Modified;
                patientEntity.UserId = patient.UserId;
                patientEntity.Weight = patient.Weight;
                patientEntity.Height = patient.Height;
                patientEntity.Occupation = patient.Occupation;
                patientEntity.Complains = patient.Complains;
                //if (!string.IsNullOrWhiteSpace(doctor.DocScan))
                //{
                //    doctorEntity.DocScan = doctor.DocScan;
                //}

                patientEntity.TimeOfFirstAppearance = patient.TimeOfFirstAppearance;
                patientEntity.Survey = patient.Survey;
                patientEntity.Diagnosis = patient.Diagnosis;
                patientEntity.Treatment = patient.Treatment;
                patientEntity.Surgery = patient.Surgery;
                patientEntity.MoreInfo = patient.MoreInfo;
                patientEntity.Allergy = patient.Allergy;
                patientEntity.PhysicalExaminationData = patient.PhysicalExaminationData;
                patientEntity.IsActive = patient.IsActive;


                patientEntity.UserProfile.FirstName = userProfileDto.FirstName;
                patientEntity.UserProfile.SecondName = userProfileDto.SecondName;
                patientEntity.UserProfile.Surname = userProfileDto.Surname;
                patientEntity.UserProfile.Age = userProfileDto.Age;
                patientEntity.UserProfile.GenderId = userProfileDto.GenderId;
                patientEntity.Foto = patient.Foto;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            SetDropDownsOnEdit(patient);
            //ViewBag.UserId = new SelectList(_db.UserProfiles, "UserId", "UserName", patient.UserId);
            ViewBag.Gender = SetGenders(userProfileDto.GenderId);
            return View(patient);
        }

        private void SetDropDownsOnEdit(Patient patient)
        {
            ViewBag.SymptomsList = new MultiSelectList(_db.Symptoms, "SymptomId", "Name",
                patient.Symptom.Select(s => s.SymptomId.ToString(CultureInfo.InvariantCulture)).ToList());
            var userIds = _db.Patients.Where(p => p.UserId != patient.UserId).Select(p => p.UserId).ToList();
            var users = new List<UserProfile>();
            foreach (var userProfile in _db.UserProfiles.Include("Roles").Where(userProfile => !userIds.Contains(userProfile.UserId)))
            {
                users.AddRange(from role in userProfile.Roles where role.RoleName == "Patient" select userProfile);
            }
            ViewBag.UserId = new SelectList(users, "UserId",
                                            "UserName", patient.UserId);
            ViewBag.ConfirmationList = GetConfirmationList(patient.IsActive);
        }

        //
        // GET: /Patient/Delete/5

        public ActionResult Delete(int id = 0)
        {
            //var patient = _db.Patients.Find(id);
            var patient = _db.Patients
                .Include(p => p.UserProfile)
                .FirstOrDefault(u => u.PatientId == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            
            return View(patient);
        }

        //
        // POST: /Patient/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            //var patient = _db.Patients.Find(id);
            var patient = _db.Patients
                .Include(p => p.UserProfile)
                .Include(p => p.Advices)
                .FirstOrDefault(u => u.PatientId == id);
            if (patient != null && patient.Advices.Any())
            {
                ModelState.AddModelError("Common", "Удаление невозможно, у пациента имеются консультации");
                return View(patient);
            }
            _db.Patients.Remove(patient);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin, Patient")]
        [ChildActionOnly]
        public ActionResult UploadFiles(int? id)
        {
            Patient patient;
            if (id.HasValue)
            {
                patient = _db.Patients.FirstOrDefault(p => p.PatientId == id.Value);
            }
            else
            {
                var user = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
                patient = _db.Patients.FirstOrDefault(p => p.UserId == user.UserId);
            }
            if (patient == null)
            {
                return HttpNotFound();
            }
            return PartialView("_UploadFilesPartial",
                               new UploadSettings
                                   {
                                       UploadOwnerId = patient.PatientId,
                                       UploadOwnerType = UploadOwnerType.Patient.ToString()
                                   });

        }

        // GET: /Patient/EditPersonalArea
        [Authorize(Roles = "Admin, Patient")]
        public ActionResult EditPersonalArea()
        {
            //var doctor = _db.Doctors.Find(id);
            
            var currentUser = _db.UserProfiles
                .Include(u => u.Gender)
                .FirstOrDefault(u => u.UserName == User.Identity.Name);
            Patient patient = null;
            int? genderId = null;
            if (currentUser != null)
            {
                
                genderId = currentUser.Gender != null ? currentUser.Gender.GenderId : (int?)null;
                patient = _db.Patients.Include(p => p.UserProfile).FirstOrDefault(p => p.UserId == currentUser.UserId);
                if (patient == null)
                {
                    return HttpNotFound();
                }
            }
            //SetDropDownsOnEdit(doctor);
            ViewBag.Gender = SetGenders(genderId);
            return View(patient);
        }

        private SelectList SetGenders(int? id)
        {
            var genders = new List<object>();
            foreach (var gender in _db.Genders)
            {
                genders.Add(new { Id = gender.GenderId, Name = gender.GetFullName() });
            }
            return id.HasValue ? new SelectList(genders, "Id", "Name", id.Value) : new SelectList(genders, "Id", "Name");
        }

        // POST: /Patient/EditPersonalArea
        [Authorize(Roles = "Admin, Patient")]
        [HttpPost]
        public ActionResult EditPersonalArea(Patient patient, UserProfileDto userProfileDto)
        {
            var patientEntity = _db.Patients
                .Include(p => p.UserProfile)
                .FirstOrDefault(p => p.PatientId == patient.PatientId);
            //try
            //{

            if (ModelState.IsValid)
            {

                //if (!SaveDoctorUploads(doctorEntity)) return ResultOnErrorEdit(doctorEntity);

                if (patientEntity == null) return RedirectToAction("Index");

                patientEntity.Height = patient.Height;
                patientEntity.Weight = patient.Weight;
                patientEntity.Occupation = patient.Occupation;
                patientEntity.Complains = patient.Complains;
                //if (!string.IsNullOrWhiteSpace(doctor.DocScan))
                //{
                //    doctorEntity.DocScan = doctor.DocScan;
                //}

                patientEntity.TimeOfFirstAppearance = patient.TimeOfFirstAppearance;
                patientEntity.Survey = patient.Survey;
                patientEntity.Diagnosis = patient.Diagnosis;
                patientEntity.Treatment = patient.Treatment;
                patientEntity.Surgery = patient.Surgery;
                patientEntity.MoreInfo = patient.MoreInfo;
                patientEntity.Allergy = patient.Allergy;
                patientEntity.PhysicalExaminationData = patient.PhysicalExaminationData;


                patientEntity.UserProfile.FirstName = userProfileDto.FirstName;
                patientEntity.UserProfile.SecondName = userProfileDto.SecondName;
                patientEntity.UserProfile.Surname = userProfileDto.Surname;
                patientEntity.UserProfile.Age = userProfileDto.Age;
                patientEntity.UserProfile.GenderId = userProfileDto.GenderId;
                patientEntity.Foto = patient.Foto;
                _db.SaveChanges();
                return RedirectToAction("PersonalArea");
            }
            //SetDropDownsOnEdit(doctorEntity);
            ViewBag.Gender = SetGenders(userProfileDto.GenderId);
            return View(patientEntity);
            //}
            //catch (Exception ex)
            //{

            //    ModelState.AddModelError("Common", ex.Message);
            //    return ResultOnError(doctorEntity);
            //}
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}