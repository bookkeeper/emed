﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Models;
using Microsoft.Web.WebPages.OAuth;

namespace EMedSovet.Web.Controllers
{
    public class HomeController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            
            
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        public ActionResult Payment()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult TermsConditions()
        {

            return View();
        }
        
        [Authorize]
        public ActionResult PersonalCard(string role)
        {
            
            return RedirectToAction(role == "doctor" ? "DoctorCard" : "PatientCard");
        }

        public ActionResult DoctorCard()
        {

            return View();
        }

        public ActionResult PatientCard()
        {

            return View();
        }


        public ActionResult MoreInfo()
        {
            return View();
        }
        public ActionResult HowToConsult()
        {
            return View();
        }
        public ActionResult ConsultTypes()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Chat()
        {
            UserProfile user = null;
            var avatarUrl = Url.Content("~/images/33.jpg");
            if (Request.IsAuthenticated)
            {
                if (User.IsInRole("Doctor"))
                {
                    user = _db.UserProfiles
                        .Include("Doctor")
                        .FirstOrDefault(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        var doctor = user.Doctor.FirstOrDefault();
                        if (doctor != null && !string.IsNullOrWhiteSpace(doctor.Foto))
                        {
                            avatarUrl =
                           Url.Content(string.Format("~/Files/Doctor/{0}/Photo/{1}", doctor.DoctorId, doctor.Foto));
                        }
                       
                    }
                    
                }
                if (User.IsInRole("Patient"))
                {
                    user = _db.UserProfiles
                        .Include("Patient")
                        .FirstOrDefault(u => u.UserName == User.Identity.Name);
                    if (user != null)
                    {
                        var patient = user.Patient.FirstOrDefault();
                        if (patient != null && !string.IsNullOrWhiteSpace(patient.Foto))
                        {
                            avatarUrl =
                           Url.Content(string.Format("~/Files/Patient/{0}/Photo/{1}", patient.PatientId, patient.Foto));
                        }
                    }
                }
                ViewBag.AvatarUrl = avatarUrl;

            }
            return View(user);
        }
        public ActionResult VideoChat()
        {
            return View();
        }

    }
}
