﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Filters;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [InitializeSimpleMembership]
    public class SpecialtyController : Controller
    {
        private EMedSovetEntities db = new EMedSovetEntities();

        //
        // GET: /Specialty/

        public ActionResult Index()
        {
            return View(db.Specialties.ToList());
        }

        //
        // GET: /Specialty/Details/5

        public ActionResult Details(int id = 0)
        {
            Specialty specialty = db.Specialties.Find(id);
            if (specialty == null)
            {
                return HttpNotFound();
            }
            return View(specialty);
        }

        //
        // GET: /Specialty/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Specialty/Create

        [HttpPost]
        public ActionResult Create(Specialty specialty)
        {
            if (ModelState.IsValid)
            {
                db.Specialties.Add(specialty);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(specialty);
        }

        //
        // GET: /Specialty/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Specialty specialty = db.Specialties.Find(id);
            if (specialty == null)
            {
                return HttpNotFound();
            }
            return View(specialty);
        }

        //
        // POST: /Specialty/Edit/5

        [HttpPost]
        public ActionResult Edit(Specialty specialty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(specialty);
        }

        //
        // GET: /Specialty/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Specialty specialty = db.Specialties.Find(id);
            if (specialty == null)
            {
                return HttpNotFound();
            }
            return View(specialty);
        }

        //
        // POST: /Specialty/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Specialty specialty = db.Specialties.Find(id);
            db.Specialties.Remove(specialty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}