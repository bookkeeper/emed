﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Controllers
{
    public class DiseaseController : Controller
    {
        private EMedSovetEntities db = new EMedSovetEntities();

        //
        // GET: /Disease/

        public ActionResult Index()
        {
            return View(db.Diseases.ToList());
        }

        //
        // GET: /Disease/Details/5

        public ActionResult Details(int id = 0)
        {
            Disease disease = db.Diseases.Find(id);
            if (disease == null)
            {
                return HttpNotFound();
            }
            return View(disease);
        }

        //
        // GET: /Disease/Create

        public ActionResult Create()
        {
            ViewBag.Articles = new SelectList(GetArticleList());
            return View();
        }

        //
        // POST: /Disease/Create

        [HttpPost]
        public ActionResult Create(Disease disease)
        {
            ViewBag.Articles = new SelectList(GetArticleList());
            if (ModelState.IsValid)
            {
                HttpPostedFileBase hpf = null;
                foreach (string input in Request.Files)
                {
                    hpf = Request.Files[input];
                    if (hpf == null) continue;
                    if (hpf.ContentLength == 0)
                        continue;
                    var fileInfo = new FileInfo(hpf.FileName);
                    if (fileInfo.Extension != ".html") hpf = null;
                }
                if (hpf == null)
                {
                    ModelState.AddModelError("Common", "File can be only html type");
                    return View(disease);
                }
                if (hpf.ContentLength != 0)
                {
                    var articleDirectoryPath = string.Format("{0}\\{1}\\",
                                                        ControllerContext.HttpContext.Server.MapPath(
                                                            ControllerContext.HttpContext.Request.ApplicationPath), "Articles");
                    if (!Directory.Exists(articleDirectoryPath)) Directory.CreateDirectory(articleDirectoryPath);
                    var articleList = GetArticleList();
                    if (articleList.Contains(hpf.FileName))
                    {
                        ModelState.AddModelError("Common", "File with this name already exists");
                        return View(disease);
                    }

                    var savedFileName = Path.Combine(articleDirectoryPath, hpf.FileName);

                    hpf.SaveAs(savedFileName);
                    disease.Article = hpf.FileName;
                }
                db.Diseases.Add(disease);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(disease);
        }

        //
        // GET: /Disease/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Disease disease = db.Diseases.Find(id);
            ViewBag.Articles = new SelectList(GetArticleList());
            if (disease == null)
            {

                return HttpNotFound();
            }
            return View(disease);
        }

        private IEnumerable<string> GetArticleList()
        {
            var articleDirectoryPath = string.Format("{0}\\{1}\\",
                                             ControllerContext.HttpContext.Server.MapPath(
                                                 ControllerContext.HttpContext.Request.ApplicationPath), "Articles");
            var files = new List<string>();
            if (!Directory.Exists(articleDirectoryPath)) return files;
            foreach (var file in Directory.GetFiles(articleDirectoryPath))
            {
                var fileInfo = new FileInfo(file);
                if (fileInfo.Extension == ".html")
                    files.Add(fileInfo.Name);
            }
            return files;
        }

        //
        // POST: /Disease/Edit/5

        [HttpPost]
        public ActionResult Edit(Disease disease)
        {
            if (ModelState.IsValid)
            {

                db.Entry(disease).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(disease);
        }

        //
        // GET: /Disease/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Disease disease = db.Diseases.Find(id);
            if (disease == null)
            {
                return HttpNotFound();
            }
            return View(disease);
        }

        //
        // POST: /Disease/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Disease disease = db.Diseases.Find(id);
            db.Diseases.Remove(disease);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}