﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Filters;
using EMedSovet.Web.Models;
using EMedSovet.Web.Models.Dto;
using EMedSovet.Web.Properties;
using EMedSovet.Web.Upload;

namespace EMedSovet.Web.Controllers
{

    public class DoctorController : BaseController
    {


        #region Actions

        [AllowAnonymous]
        public ActionResult DoctorsList(int? specialtyId)
        {
            var specialties = _db.Specialties.OrderBy(s => s.Name);
            var diseases = _db.Diseases.OrderBy(d => d.Name);
            var doctors = _db.Doctors.Include(d => d.UserProfile).Include(d => d.Degree);
            ViewBag.Specialties = GetGroupList(specialties);
            ViewBag.Diseases = GetGroupList(diseases);
            ViewBag.Doctors = GetGroupList(doctors);
            var doctorsEntitiesList = new List<Doctor>(); ;
            if (specialtyId != null)
            {
                doctorsEntitiesList.AddRange(
                    _db.Doctors.Include(d => d.Specialty).Include(d => d.Degree).OrderBy(d => d.Degree.Priority)
                       .Where(doctor => doctor.Specialty.Any(specialty => specialty.SpecialtyId == specialtyId.Value)));
            }
            else
            {
                doctorsEntitiesList.AddRange(_db.Doctors.OrderBy(d => d.Degree.Priority));
            }


            return View(doctorsEntitiesList);
        }

        [AllowAnonymous]
        public ActionResult BookmarkDoctor()
        {
            return View();
        }
        //
        // GET: /Doctor/
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            //var doctor = _db.Doctors.Include(d => d.Specialty).Include(d => d.UserProfile);
            var doctor = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .Include(d => d.Degree);
            return View(doctor.ToList());
        }

        //
        // GET: /Doctor/Info
        [Authorize(Roles = "Admin, Doctor")]
        public ActionResult Info()
        {

            return View();
        }

        //
        // GET: /Doctor/PersonalArea
        [Authorize(Roles = "Admin, Doctor")]
        public ActionResult PersonalArea()
        {
            var currentUser = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
            Doctor doctor = null;
            if (currentUser != null)
            {
                doctor = _db.Doctors
                    .Include(d => d.Specialty)
                    .Include(d => d.Degree)
                    .Include(d => d.Advices.Select(a => a.AdviceStatus))
                    .Include(d => d.Advices.Select(a => a.Patient).Select(p => p.UserProfile))
                    .FirstOrDefault(d => d.UserId == currentUser.UserId);
                if (doctor == null)
                {
                    return HttpNotFound();
                }
            }

            return View(doctor);
        }



        // GET: /Doctor/EditPersonalArea
        [Authorize(Roles = "Admin, Doctor")]
        public ActionResult EditPersonalArea()
        {
            //var doctor = _db.Doctors.Find(id);
            var currentUser = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
            Doctor doctor = null;
            if (currentUser != null)
            {
                doctor = _db.Doctors.Include(d => d.Specialty)
                    .Include(d => d.UserProfile)
                    .Include(d => d.Degree)
                    .FirstOrDefault(d => d.UserId == currentUser.UserId);
                if (doctor == null)
                {
                    return HttpNotFound();
                }
            }
            SetDropDownsOnEdit(doctor);
            return View(doctor);
        }

        // POST: /Doctor/EditPersonalArea/5
        [Authorize(Roles = "Admin, Doctor")]
        [HttpPost]
        public ActionResult EditPersonalArea(Doctor doctor, DoctorDto doctorDto, UserProfileDto userProfileDto)
        {
            var doctorEntity = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .FirstOrDefault(d => d.DoctorId == doctor.DoctorId);
            //try
            //{

            if (ModelState.IsValid)
            {

                //if (!SaveDoctorUploads(doctorEntity)) return ResultOnErrorEdit(doctorEntity);

                if (doctorEntity == null) return RedirectToAction("Index");
                doctorEntity.Specialty.Clear();
                //var specialties = collection["Specialties"];
                var specialtiesList = GetSpecialties(doctorDto.Specialties).ToList();
                foreach (var specialty in specialtiesList)
                {
                    doctorEntity.Specialty.Add(specialty);
                }

                doctorEntity.Education = doctor.Education;
                doctorEntity.DegreeId = doctor.DegreeId;
                //if (!string.IsNullOrWhiteSpace(doctor.DocScan))
                //{
                //    doctorEntity.DocScan = doctor.DocScan;
                //}

                doctorEntity.ConsultationArea = doctor.ConsultationArea;
                doctorEntity.Resume = doctor.Resume;
                doctorEntity.Summary = doctor.Summary;
                doctorEntity.Experience = doctor.Experience;
                
                doctorEntity.UserProfile.FirstName = userProfileDto.FirstName;
                doctorEntity.UserProfile.SecondName = userProfileDto.SecondName;
                doctorEntity.UserProfile.Surname = userProfileDto.Surname;
                doctorEntity.Foto = doctor.Foto;
                //if (!string.IsNullOrWhiteSpace(doctor.Foto))
                //{
                //    doctorEntity.Foto = doctor.Foto;
                //}
                //doctorEntity.IsConfirmed = doctor.IsConfirmed;
                //doctorEntity.UserId = doctor.UserId;



                //doctor.Specialty.Clear();
                //foreach (var specialty in specialtiesList)
                //{
                //    doctor.Specialty.Add(specialty);
                //}
                //_db.Doctors.Attach(doctor);
                //_db.Entry(doctor).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("DoctorCardInternal");
            }
            SetDropDownsOnEdit(doctorEntity);
            return View(doctorEntity);
            //}
            //catch (Exception ex)
            //{

            //    ModelState.AddModelError("Common", ex.Message);
            //    return ResultOnError(doctorEntity);
            //}
        }

        //
        // GET: /Doctor/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            //var doctor = _db.Doctors.Find(id);
            var doctor = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .Include(d => d.Degree)
                .FirstOrDefault(u => u.DoctorId == id);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            return View(doctor);
        }

        //
        // GET: /Doctor/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            SetDropDownsOnCreate();
            return View();
        }

        //
        // POST: /Doctor/Create

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(Doctor doctor, DoctorDto doctorDto, UserProfileDto userProfileDto)
        {
            try
            {
                //if (!SaveDoctorUploads(doctor)) return ResultOnError(doctor);
                var specialtiesList = GetSpecialties(doctorDto.Specialties);
                doctor.Specialty.Clear();
                foreach (var specialty in specialtiesList)
                {
                    doctor.Specialty.Add(specialty);

                }
                var user = _db.UserProfiles.FirstOrDefault(u => u.UserId == doctor.UserId);
                if (user != null)
                {
                    user.FirstName = userProfileDto.FirstName;
                    user.SecondName = userProfileDto.SecondName;
                    user.Surname = userProfileDto.Surname;
                }

                if (ModelState.IsValid)
                {
                    _db.Doctors.Add(doctor);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return ResultOnError(doctor);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Common", ex.Message);
                return ResultOnError(doctor);
            }
        }



        //
        // GET: /Doctor/DoctorCard
        [AllowAnonymous]
        public ActionResult DoctorCard(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var doctor = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .Include(d => d.Degree)
                .FirstOrDefault(u => u.DoctorId == id.Value);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            return View(doctor);
        }

        //
        // GET: /Doctor/DoctorCard
        [Authorize(Roles = "Admin, Doctor")]
        public ActionResult DoctorCardInternal()
        {
            var currentUser = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
            Doctor doctor = null;
            if (currentUser != null)
            {
                doctor = _db.Doctors
                    .Include(d => d.Specialty)
                    .Include(d => d.Degree)
                    .FirstOrDefault(d => d.UserId == currentUser.UserId);
                if (doctor == null)
                {
                    return HttpNotFound();
                }
            }
            return View(doctor);
        }

        //
        // GET: /Doctor/Create
        [Authorize(Roles = "Doctor")]
        public ActionResult FillDoctorsCard()
        {
            SetDropDownsOnFillDoctorCard();
            return View();
        }

        //
        // POST: /Doctor/Create
        [Authorize(Roles = "Doctor")]
        [HttpPost]
        public ActionResult FillDoctorsCard(Doctor doctor, FormCollection collection)
        {
            try
            {
                UpdateUserDetails(collection);

                if (!SaveDoctorUploads(doctor)) return ResultOnError(doctor);
                if (ModelState.IsValid)
                {
                    _db.Doctors.Add(doctor);
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return ResultOnError(doctor);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Common", ex.Message);
                return ResultOnError(doctor);
            }
        }

        //
        // GET: /Doctor/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            //var doctor = _db.Doctors.Find(id);
            var doctor = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .Include(d => d.Degree)
                .FirstOrDefault(u => u.DoctorId == id);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            SetDropDownsOnEdit(doctor);
            return View(doctor);
        }

        //
        // POST: /Doctor/Edit/5

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Doctor doctor, DoctorDto doctorDto, UserProfileDto userProfileDto)
        {
            var doctorEntity = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .FirstOrDefault(d => d.DoctorId == doctor.DoctorId);
            try
            {

                if (ModelState.IsValid)
                {

                    //if (!SaveDoctorUploads(doctorEntity)) return ResultOnErrorEdit(doctorEntity);

                    if (doctorEntity == null) return RedirectToAction("Index");
                    doctorEntity.Specialty.Clear();
                    var specialtiesList = GetSpecialties(doctorDto.Specialties).ToList();
                    foreach (var specialty in specialtiesList)
                    {
                        doctorEntity.Specialty.Add(specialty);
                    }


                    doctorEntity.Education = doctor.Education;
                    doctorEntity.DegreeId = doctor.DegreeId;
                    //if (!string.IsNullOrWhiteSpace(doctor.DocScan))
                    //{
                    //    doctorEntity.DocScan = doctor.DocScan;
                    //}

                    doctorEntity.ConsultationArea = doctor.ConsultationArea;
                    doctorEntity.Resume = doctor.Resume;
                    doctorEntity.Summary = doctor.Summary;
                    doctorEntity.Experience = doctor.Experience;
                    doctorEntity.IsActive = doctor.IsActive;
                    doctorEntity.UserProfile.FirstName = userProfileDto.FirstName;
                    doctorEntity.UserProfile.SecondName = userProfileDto.SecondName;
                    doctorEntity.UserProfile.Surname = userProfileDto.Surname;
                    doctorEntity.Foto = doctor.Foto;

                    //doctor.Specialty.Clear();
                    //foreach (var specialty in specialtiesList)
                    //{
                    //    doctor.Specialty.Add(specialty);
                    //}
                    //_db.Doctors.Attach(doctor);
                    //_db.Entry(doctor).State = EntityState.Modified;
                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }
                SetDropDownsOnEdit(doctorEntity);
                return View(doctorEntity);
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("Common", ex.Message);
                return ResultOnError(doctorEntity);
            }
        }

        //
        // GET: /Doctor/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            //Doctor doctor = _db.Doctors.Find(id);
            var doctor = _db.Doctors
                .Include(d => d.Specialty)
                .Include(d => d.UserProfile)
                .Include(d => d.Degree)
                .FirstOrDefault(u => u.DoctorId == id);
            if (doctor == null)
            {
                return HttpNotFound();
            }
            return View(doctor);
        }

        //
        // POST: /Doctor/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Doctor doctor = _db.Doctors.Find(id);
            _db.Doctors.Remove(doctor);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }


        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult DoctorsListCarousel()
        {
            return PartialView("_DoctorsListCarousel",
                               _db.Doctors
                               .Include(d => d.Specialty)
                               .Include(d => d.UserProfile)
                               .Include(d => d.Degree)
                               .ToList()
                               .Where(d => d.IsConfirmed == true && !string.IsNullOrWhiteSpace(d.Foto) && d.IsActive.HasValue && d.IsActive.Value)
                               .OrderBy(d => d.Degree.Priority).ToList());
        }

        [Authorize(Roles = "Admin, Doctor")]
        [ChildActionOnly]
        public ActionResult UploadFiles(int? id)
        {
            Doctor doctor;
            if (id.HasValue)
            {
                doctor = _db.Doctors.FirstOrDefault(d => d.DoctorId == id.Value);
                //user = _db.UserProfiles.FirstOrDefault(u => u.UserId == id.Value);
            }
            else
            {
                var user = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
                doctor = _db.Doctors.FirstOrDefault(d => d.UserId == user.UserId);
            }

            if (doctor == null)
            {
                return HttpNotFound();
            }
            return PartialView("_UploadFilesPartial",
                                          new UploadSettings { UploadOwnerId = doctor.DoctorId, UploadOwnerType = UploadOwnerType.Doctor.ToString() });
            //return Content(string.Empty);

        }

        //
        // GET: /Doctor/DoctorChat
        [Authorize(Roles = "Admin, Doctor")]
        public ActionResult DoctorChat()
        {
            return View();
        }
        #endregion Actions


        #region protected
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
        #endregion protected


        #region private

        private void SetDropDownsOnCreate()
        {
            ViewBag.SpecialtiesList = new MultiSelectList(_db.Specialties, "SpecialtyId", "Name");
            var userIds = _db.Doctors.ToList().Select(doc => doc.UserId).ToList();
            var users = new List<UserProfile>();
            foreach (var userProfile in _db.UserProfiles.Include("Roles").Where(userProfile => !userIds.Contains(userProfile.UserId)))
            {
                users.AddRange(from role in userProfile.Roles where role.RoleName == "Doctor" select userProfile);
            }

            ViewBag.UserId = new SelectList(users, "UserId", "UserName");
            ViewBag.ConfirmationList = GetConfirmationList(false);
            var degrees = _db.Degrees.ToList().OrderBy(d => d.Name).ToList();
            ViewBag.Degrees = new SelectList(degrees, "DegreeId", "Name");
        }

        private void SetDropDownsOnEdit(Doctor doctor)
        {
            //ViewBag.SpecialtyId = new SelectList(_db.Specialties, "SpecialtyId", "Name", doctor.SpecialtyId);
            ViewBag.SpecialtiesList = new MultiSelectList(_db.Specialties, "SpecialtyId", "Name",
                                                          doctor.Specialty.Select(
                                                              s => s.SpecialtyId.ToString(CultureInfo.InvariantCulture))
                                                                .ToList());
            //ViewBag.SpecialtyId = new SelectList(_db.Specialties, "SpecialtyId", "Name", 1);
            var userIds = _db.Doctors.Where(d => d.UserId != doctor.UserId).Select(doc => doc.UserId).ToList();
            var users = new List<UserProfile>();
            foreach (var userProfile in _db.UserProfiles.Include("Roles").Where(userProfile => !userIds.Contains(userProfile.UserId)))
            {
                users.AddRange(from role in userProfile.Roles where role.RoleName == "Doctor" select userProfile);
            }
            ViewBag.UserId = new SelectList(users, "UserId",
                                            "UserName", doctor.UserId);
            ViewBag.ConfirmationList = GetConfirmationList(doctor.IsActive);
            var degrees = _db.Degrees.ToList().OrderBy(d => d.Name).ToList();
            ViewBag.Degrees = new SelectList(degrees, "DegreeId", "Name", doctor.DegreeId);
        }

        private void SetDropDownsOnFillDoctorCard()
        {
            var currentUser = _db.UserProfiles.FirstOrDefault(u => u.UserName == User.Identity.Name);
            var currentUserId = currentUser != null ? currentUser.UserId : 0;
            ViewBag.CurrentUserId = currentUserId;
            ViewBag.SpecialtiesList = new MultiSelectList(_db.Specialties, "SpecialtyId", "Name");

            //var membeships = _db.Memberships.Include("Roles").ToList();
            var usersDoctors = _db.UserProfiles.
                Include(u => u.Roles)
                .ToList();
            var userIds =
                (from user in usersDoctors
                 where user.Roles.Any(role => role.RoleName == "Doctor")
                 select user.UserId).ToList();
            var users =
                _db.UserProfiles.
                Include(u => u.Doctor)
                .Where(user => userIds.Contains(user.UserId) && !user.Doctor.Any());

            ViewBag.UserId = new SelectList(users, "UserId",
                                            "UserName", currentUserId);
            ViewBag.ConfirmationList = GetConfirmationList(false);
            ViewBag.AgesList = GetAgesList();
            ViewBag.GendersList = GetGendersList();
        }

        private void UpdateUserDetails(FormCollection collection)
        {
            var userId = Convert.ToInt32(GetValueFromCollection("UserId", collection));
            var user = _db.UserProfiles.FirstOrDefault(u => u.UserId == userId);
            if (user == null) return;
            var surname = GetValueFromCollection("Surname", collection);
            var firstName = GetValueFromCollection("FirstName", collection);
            var secondName = GetValueFromCollection("SecondName", collection);
            var age = Convert.ToInt32(GetValueFromCollection("Age", collection));
            var genderId = Convert.ToInt32(GetValueFromCollection("GenderId", collection));
            var email = GetValueFromCollection("Email", collection);
            user.Surname = surname;
            user.FirstName = firstName;
            user.SecondName = secondName;
            user.Age = age;
            user.GenderId = genderId;
            user.Email = email;
        }

        private static string GetValueFromCollection(string inputName, NameValueCollection collection)
        {
            foreach (var key in collection.AllKeys.Where(key => key.Contains(inputName)))
            {
                return collection.Get(key);
            }
            return string.Empty;
        }



        private bool SaveDoctorUploads(Doctor doctor)
        {


            var uploadedFileName = GetUploadedFileName(FOTO_INPUT_NAME);
            if (!string.IsNullOrWhiteSpace(uploadedFileName))
            {
                if (IsIconSizeExceeded(FOTO_INPUT_NAME))
                {
                    ModelState.AddModelError("Common", "Icon size too large, cannot be more than 1MB");
                    return false;
                }
                var uploadPathFoto = GetFotoPathUploadByUserId(doctor.UserId);
                var userUploadDir = string.Format("{0}{1}\\{2}", AppDomain.CurrentDomain.BaseDirectory, uploadPathFoto.TrimStart(new[] { '\\' }),
                                              uploadedFileName);
                if (System.IO.File.Exists(userUploadDir))
                {
                    ModelState.AddModelError("Common", string.Format("File {0} already exists", uploadedFileName));
                    return false;
                }
                SaveFile(FOTO_INPUT_NAME, uploadPathFoto);
                doctor.Foto = GetUploadedFileName(FOTO_INPUT_NAME);
            }


            uploadedFileName = GetUploadedFileName(DOC_SCAN_INPUT_NAME);
            if (!string.IsNullOrWhiteSpace(uploadedFileName))
            {
                if (IsIconSizeExceeded(DOC_SCAN_INPUT_NAME))
                {
                    ModelState.AddModelError("Common", "File size too large, cannot be more than 1MB");
                    {
                        return false;
                    }
                }
                var uploadPathDoc = GetDocPathUploadByUserId(doctor.UserId);

                var userUploadDir = string.Format("{0}{1}\\{2}", AppDomain.CurrentDomain.BaseDirectory, uploadPathDoc.TrimStart(new[] { '\\' }),
                                                  uploadedFileName);
                if (System.IO.File.Exists(userUploadDir))
                {
                    ModelState.AddModelError("Common", string.Format("File {0} already exists", uploadedFileName));
                    return false;
                }
                SaveFile(DOC_SCAN_INPUT_NAME, uploadPathDoc);
                doctor.DocScan = GetUploadedFileName(DOC_SCAN_INPUT_NAME);

            }

            return true;
        }

        private static string GetDocPathUploadByUserId(int userId)
        {
            var userUploadDir = string.Format("{0}Uploads\\Doctors\\{1}", AppDomain.CurrentDomain.BaseDirectory, userId);
            if (!Directory.Exists(userUploadDir)) Directory.CreateDirectory(userUploadDir);
            var userUploadDirDocs = string.Format("{0}\\Docs", userUploadDir);
            if (!Directory.Exists(userUploadDirDocs)) Directory.CreateDirectory(userUploadDirDocs);
            //return userUploadDirDocs;
            return string.Format("\\Uploads\\Doctors\\{0}\\Docs", userId);
        }

        private static string GetFotoPathUploadByUserId(int userId)
        {
            var userUploadDir = string.Format("{0}Uploads\\Doctors\\{1}", AppDomain.CurrentDomain.BaseDirectory, userId);
            if (!Directory.Exists(userUploadDir)) Directory.CreateDirectory(userUploadDir);
            var userUploadDirFoto = string.Format("{0}\\Photo", userUploadDir);
            if (!Directory.Exists(userUploadDirFoto)) Directory.CreateDirectory(userUploadDirFoto);
            return string.Format("\\Uploads\\Doctors\\{0}\\Photo", userId);
        }

        private ActionResult ResultOnError(Doctor doctor)
        {
            SetDropDownsOnCreate();
            return View(doctor);
        }

        private ActionResult ResultOnErrorEdit(Doctor doctor)
        {
            SetDropDownsOnEdit(doctor);
            return View(doctor);
        }

        private IEnumerable<Specialty> GetSpecialties(string specialties)
        {
            var specialtiesList = new List<Specialty>();
            if (string.IsNullOrWhiteSpace(specialties)) return specialtiesList;
            foreach (var specialty in specialties.Split(new[] { ',' }))
            {
                var specialtyId = Convert.ToInt32(specialty);
                var specialtyEntity = _db.Specialties.FirstOrDefault(s => s.SpecialtyId == specialtyId);
                if (specialty != null)
                {
                    specialtiesList.Add(specialtyEntity);
                }
            }
            return specialtiesList;
        }

        private SelectList GetAgesList()
        {
            var ages = Enumerable.Range(10, 120).ToList();
            return new SelectList(ages, 30);
        }
        private SelectList GetGendersList()
        {
            var genders = _db.Genders;
            var gendersList = new List<object>();
            foreach (var gender in genders)
            {
                gendersList.Add(new { gender.GenderId, Name = GetGenderName(gender.Name) });
            }
            return new SelectList(gendersList, "GenderId", "Name");
        }

        private static string GetGenderName(string name)
        {
            switch (name)
            {
                case "M":
                    return "Муж";
                case "F":
                    return "Жен";
                default:
                    return string.Empty;
            }
        }
        #endregion private

    }
}