namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSpecialityDosctorsRelationship : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SpecialtyDoctors",
                c => new
                {
                    Specialty_SpecialtyId = c.Int(nullable: false),
                    Doctor_DoctorId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.Specialty_SpecialtyId, t.Doctor_DoctorId })
                .ForeignKey("dbo.Specialties", t => t.Specialty_SpecialtyId, cascadeDelete: true)
                .ForeignKey("dbo.Doctors", t => t.Doctor_DoctorId, cascadeDelete: true)
                .Index(t => t.Specialty_SpecialtyId)
                .Index(t => t.Doctor_DoctorId);
        }
        
        public override void Down()
        {
            DropIndex("dbo.SpecialtyDoctors", new[] { "Doctor_DoctorId" });
            DropIndex("dbo.SpecialtyDoctors", new[] { "Specialty_SpecialtyId" });
            DropForeignKey("dbo.SpecialtyDoctors", "Doctor_DoctorId", "dbo.Doctors");
            DropForeignKey("dbo.SpecialtyDoctors", "Specialty_SpecialtyId", "dbo.Specialties");
            DropTable("dbo.SpecialtyDoctors");
        }
    }
}
