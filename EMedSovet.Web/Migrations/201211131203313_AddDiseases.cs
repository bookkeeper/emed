namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDiseases : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Diseases",
                c => new
                    {
                        DiseaseId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Description = c.String(),
                        Article = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.DiseaseId);
        }
        
        public override void Down()
        {
            DropTable("dbo.Diseases");
        }
    }
}
