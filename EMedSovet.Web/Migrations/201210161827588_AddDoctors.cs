namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDoctors : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Doctors",
                c => new
                {
                    DoctorId = c.Int(nullable: false, identity: true),
                    DegreeId = c.Int(),
                    ConsultationArea = c.String(maxLength: 500),
                    Education = c.String(maxLength: 500),
                    Resume = c.String(maxLength: 1000),
                    Summary = c.String(),
                    Experience = c.String(),
                    UserId = c.Int(nullable: false),
                    IsConfirmed = c.Boolean(nullable: true, defaultValue: false),
                    Foto = c.String(maxLength: 500),
                    DocScan = c.String(maxLength: 500),
                    IsActive = c.Boolean(nullable: true, defaultValue: false),
                })
                .PrimaryKey(t => t.DoctorId)
                //.ForeignKey("dbo.Specialties", t => t.SpecialtyId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserId, cascadeDelete: true)
                //.Index(t => t.SpecialtyId)
                .Index(t => t.UserId);

        }

        public override void Down()
        {
            DropIndex("dbo.Doctors", new[] { "UserId" });
            //DropIndex("dbo.Doctors", new[] { "SpecialtyId" });
            DropForeignKey("dbo.Doctors", "UserId", "dbo.UserProfile");
            //DropForeignKey("dbo.Doctors", "SpecialtyId", "dbo.Specialties");
            DropTable("dbo.Doctors");
        }
    }
}
