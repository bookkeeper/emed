namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDegrees : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Degrees",
                c => new
                    {
                        DegreeId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Priority = c.Int(),
                    })
                .PrimaryKey(t => t.DegreeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Degrees");
        }
    }
}
