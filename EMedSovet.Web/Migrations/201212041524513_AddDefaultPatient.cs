using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultPatient : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Patients.AddOrUpdate(new[]
                {
                    new Patient
                        {
                            IsActive = true,
                            Allergy = string.Empty,
                            AssociatedDiseases = string.Empty,
                            AssociatedTherapy = string.Empty,
                            Complains = @"������ �������� ����, �������� ������, ������ - �������, 
                            ������ ����� ����� �������.",
                            Diagnosis = @"�������",
                            Height = 178,
                            Occupation = string.Empty,
                            PhysicalExaminationData = string.Empty,
                            Surgery = string.Empty,
                            Treatment = @"��������",
                            Weight = 56,
                            TimeOfFirstAppearance = @"����� �������� �����",
                            MoreInfo = @"���� ����� �������� ����������� � ������ �������� ���.",
                            Survey = @"����� � ���������",
                            Foto = @"patient.jpg",
                            UserProfile = context.UserProfiles.FirstOrDefault(u => u.UserName == "patient11")

                        },
                        new Patient
                        {
                            IsActive = true,
                            Allergy = string.Empty,
                            AssociatedDiseases = string.Empty,
                            AssociatedTherapy = string.Empty,
                            Complains = @"������ �������� ����, �������� ������, ������ - �������, 
                            ������ ����� ����� �������. ",
                            Diagnosis = string.Empty,
                            Height = 178,
                            Occupation = string.Empty,
                            PhysicalExaminationData = string.Empty,
                            Surgery = string.Empty,
                            Treatment = string.Empty,
                            Weight = 56,
                            UserProfile = context.UserProfiles.FirstOrDefault(u => u.UserName == "patient22")
                        },
                        new Patient
                        {
                            IsActive = true,
                            Allergy = string.Empty,
                            AssociatedDiseases = string.Empty,
                            AssociatedTherapy = string.Empty,
                            Complains = string.Empty,
                            Diagnosis = string.Empty,
                            Height = 178,
                            Occupation = string.Empty,
                            PhysicalExaminationData = string.Empty,
                            Surgery = string.Empty,
                            Treatment = string.Empty,
                            Weight = 56,
                            UserProfile = context.UserProfiles.FirstOrDefault(u => u.UserName == "patient33")
                        },
                        new Patient
                        {
                            IsActive = true,
                            Allergy = string.Empty,
                            AssociatedDiseases = string.Empty,
                            AssociatedTherapy = string.Empty,
                            Complains = string.Empty,
                            Diagnosis = string.Empty,
                            Height = 178,
                            Occupation = string.Empty,
                            PhysicalExaminationData = string.Empty,
                            Surgery = string.Empty,
                            Treatment = string.Empty,
                            Weight = 56,
                            UserProfile = context.UserProfiles.FirstOrDefault(u => u.UserName == "patient44")
                        },
                        new Patient
                        {
                            IsActive = true,
                            Allergy = string.Empty,
                            AssociatedDiseases = string.Empty,
                            AssociatedTherapy = string.Empty,
                            Complains = string.Empty,
                            Diagnosis = string.Empty,
                            Height = 178,
                            Occupation = string.Empty,
                            PhysicalExaminationData = string.Empty,
                            Surgery = string.Empty,
                            Treatment = string.Empty,
                            Weight = 56,
                            UserProfile = context.UserProfiles.FirstOrDefault(u => u.UserName == "patient55")
                        },
                        new Patient
                        {
                            IsActive = true,
                            Allergy = string.Empty,
                            AssociatedDiseases = string.Empty,
                            AssociatedTherapy = string.Empty,
                            Complains = string.Empty,
                            Diagnosis = string.Empty,
                            Height = 178,
                            Occupation = string.Empty,
                            PhysicalExaminationData = string.Empty,
                            Surgery = string.Empty,
                            Treatment = string.Empty,
                            Weight = 56,
                            UserProfile = context.UserProfiles.FirstOrDefault(u => u.UserName == "patient66")
                        },
                });
            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Patients");
            Sql("DBCC CHECKIDENT (\"Patients\", RESEED, 0);");
        }
    }
}
