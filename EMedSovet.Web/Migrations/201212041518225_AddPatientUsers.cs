using System.Collections.Generic;
using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPatientUsers : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();

            context.UserProfiles.AddOrUpdate(new[]
                {
                    new UserProfile
                        {
                            UserName = "patient11",
                            Age = 35,
                            Email = "patient11@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "���������",
                            SecondName = "��������",
                            Surname = "�������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Patient")
                                }
                        },
                    new UserProfile
                        {
                            UserName = "patient22",
                            Email = "patient22@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "����",
                            SecondName = "��������",
                            Surname = "������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Patient")
                                }
                        },
                        new UserProfile
                        {
                            UserName = "patient33",
                            Email = "patient33@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 2,
                            FirstName = "��������",
                            SecondName = "��������",
                            Surname = "��������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Patient")
                                }
                        },
                        new UserProfile
                        {
                            UserName = "patient44",
                            Email = "patient44@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "����",
                            SecondName = "��������",
                            Surname = "��������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Patient")
                                }
                        },
                        new UserProfile
                        {
                            UserName = "patient55",
                            Email = "patient55@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "������",
                            SecondName = "���������",
                            Surname = "�����",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Patient")
                                }
                        },
                        new UserProfile
                        {
                            UserName = "patient66",
                            Email = "patient66@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 2,
                            FirstName = "������",
                            SecondName = "������������",
                            Surname = "��������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Patient")
                                }
                        },

                });

            context.Memberships.AddOrUpdate(new[]
                {
                    new Membership
                        {
                            UserId = 8,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AMfCsG4lYyaxEtY1yfgIbV9mTkdJqPNEYBdzA6uDX4LbGQdGevhoXxueQexy3JO93w==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                        new Membership
                        {
                            UserId = 9,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "ABTlaSB59Zf32SrKbpSm6UaKR2URZ7n7tCdxG0fh6DRJD2GWZO0X7EDTh/oMNOnJpA==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                         new Membership
                        {
                            UserId = 10,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AB0qfj9gKi1KvQZU2l118msD8UIC/PFFKRHX5b+C/ZrENG9JDYSuxiAA9Bs2053B5Q==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                         new Membership
                        {
                            UserId = 11,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "ALi9CXeasn7SAp+Ubq9rpRNlvXQVNUcmVmr1PX10LsWhwRYE4AKuMV2WrWCm0ic8Zg==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                         new Membership
                        {
                            UserId = 12,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "ADdQMp8y3PNkOPElPNq0z8sq9v94W8GLEmySelyAb9oEUXdDLL/8Z4wwoE+nfG1fBQ==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                         new Membership
                        {
                            UserId = 13,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AOFgPyNfsmJXwgtB3NnUfQEjzmIx6RjoTBQobnzaIgnW+sqdlfqjuUr+dISxX77JNg==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },


                });

            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM webpages_Membership WHERE UserId IN(8,9,10,11,12,13)");
            Sql("DELETE FROM UserProfile  WHERE UserId IN(8,9,10,11,12,13)");
            Sql("DBCC CHECKIDENT (\"UserProfile\", RESEED, 7);");
        }
    }
}
