namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddSpecialty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Specialties",
                c => new
                {
                    SpecialtyId = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 500),
                })
                .PrimaryKey(t => t.SpecialtyId);

        }

        public override void Down()
        {
            DropTable("dbo.Specialties");
        }
    }
}
