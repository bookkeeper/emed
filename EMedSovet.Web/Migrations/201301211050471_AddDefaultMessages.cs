using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultMessages : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Messages.AddOrUpdate(new[]
                {
                    new Message
                        {
                            
                            To = 1,
                            Email = "vladdnc@mail.ru",
                            Subject = "subject",
                            Text = "test message",
                            CreatedAt = DateTime.Now
                        },

                });
            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Messages");
            Sql("DBCC CHECKIDENT (\"Messages\", RESEED, 0);");
        }
    }
}
