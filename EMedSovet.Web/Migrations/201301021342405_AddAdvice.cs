namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddAdvice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advices",
                c => new
                    {
                        AdviceId = c.Int(nullable: false, identity: true),
                        DoctorId = c.Int(nullable: false),
                        PatientId = c.Int(nullable: false),
                        AdviceStatusId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        CompletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.AdviceId)
                .ForeignKey("dbo.Doctors", t => t.DoctorId)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .ForeignKey("dbo.AdviceStatus", t => t.AdviceStatusId)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId)
                .Index(t => t.AdviceStatusId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Advices", new[] { "AdviceStatusId" });
            DropIndex("dbo.Advices", new[] { "PatientId" });
            DropIndex("dbo.Advices", new[] { "DoctorId" });
            DropForeignKey("dbo.Advices", "AdviceStatusId", "dbo.AdviceStatus");
            DropForeignKey("dbo.Advices", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Advices", "DoctorId", "dbo.Doctors");
            DropTable("dbo.Advices");
        }
    }
}
