using System;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultAdvice : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Advices.AddOrUpdate(new[]
                {
                    new Advice
                        {
                            DoctorId = 1,
                            PatientId = 1,
                            AdviceStatusId = 1,
                            CreatedAt = DateTime.Now
                        },
                    new Advice
                        {
                            DoctorId = 1,
                            PatientId = 2,
                            AdviceStatusId = 2,
                            CreatedAt = DateTime.Now
                        },
                    new Advice
                        {
                            DoctorId = 1,
                            PatientId = 3,
                            AdviceStatusId = 2,
                            CreatedAt = DateTime.Now
                        },
                    new Advice
                        {
                            DoctorId = 1,
                            PatientId = 4,
                            AdviceStatusId = 3,
                            CreatedAt = DateTime.Now
                        },
                    new Advice
                        {
                            DoctorId = 1,
                            PatientId = 5,
                            AdviceStatusId = 3,
                            CreatedAt = DateTime.Now
                        },
                    new Advice
                        {
                            DoctorId = 1,
                            PatientId = 6,
                            AdviceStatusId = 4,
                            CreatedAt = DateTime.Now.AddDays(-1),
                            CompletedAt = DateTime.Now
                        },

                });
            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Advices");
            Sql("DBCC CHECKIDENT (\"Advices\", RESEED, 0);");
        }
    }
}
