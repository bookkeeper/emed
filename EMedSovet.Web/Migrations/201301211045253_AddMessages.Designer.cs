// <auto-generated />
namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddMessages : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddMessages));
        
        string IMigrationMetadata.Id
        {
            get { return "201301211045253_AddMessages"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
