namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDocuments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        DocumentId = c.Int(nullable: false, identity: true),
                        OwnerId = c.Int(nullable: false),
                        FileName = c.String(nullable: false),
                        Extension = c.String(),
                        MimeType = c.String(),
                    })
                .PrimaryKey(t => t.DocumentId);
            
            CreateTable(
                "dbo.DocumentUserProfiles",
                c => new
                    {
                        Document_DocumentId = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Document_DocumentId, t.UserProfile_UserId })
                .ForeignKey("dbo.Documents", t => t.Document_DocumentId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId, cascadeDelete: true)
                .Index(t => t.Document_DocumentId)
                .Index(t => t.UserProfile_UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.DocumentUserProfiles", new[] { "UserProfile_UserId" });
            DropIndex("dbo.DocumentUserProfiles", new[] { "Document_DocumentId" });
            DropForeignKey("dbo.DocumentUserProfiles", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.DocumentUserProfiles", "Document_DocumentId", "dbo.Documents");
            DropTable("dbo.DocumentUserProfiles");
            DropTable("dbo.Documents");
        }
    }
}
