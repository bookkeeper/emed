using System.Collections.Generic;
using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConsultantUser : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();

            context.UserProfiles.AddOrUpdate(new[]
                {
                    new UserProfile
                        {
                            UserName = "consult11",
                            Age = 35,
                            Email = "consult11@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "���������",
                            SecondName = "��������",
                            Surname = "�������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Consultant")
                                }
                        },

                });

            context.Memberships.AddOrUpdate(new[]
                {
                    new Membership
                        {
                            UserId = 14,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "ADVTdmlue+yEV+gyLu5OPtAeCqOsm0nrOdlXcJCPXnk+NBo/qrUhBlFivi+PHvgPkQ==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },


                });

            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM webpages_Membership WHERE UserId IN(14)");
            Sql("DELETE FROM UserProfile  WHERE UserId IN(14)");
            Sql("DBCC CHECKIDENT (\"UserProfile\", RESEED, 13);");
        }
    }
}
