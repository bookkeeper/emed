namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDoctorDegreeManyToOneRelationship : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.Doctors", "DegreeId", "dbo.Degrees", "DegreeId", cascadeDelete: true);
            CreateIndex("dbo.Doctors", "DegreeId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Doctors", new[] { "DegreeId" });
            DropForeignKey("dbo.Doctors", "DegreeId", "dbo.Degrees");
        }
    }
}
