namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddSymptoms : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Symptoms",
                c => new
                    {
                        SymptomId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 500),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.SymptomId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Symptoms");
        }
    }
}
