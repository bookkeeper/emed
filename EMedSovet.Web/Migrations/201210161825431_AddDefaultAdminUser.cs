using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddDefaultAdminUser : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.UserProfiles.AddOrUpdate(new[]
                {
                    new UserProfile
                        {
                            UserName = "admin",
                            Email = "admin@localhost.com",
                            //Email = "lvvahrusheva@gmail.com",
                            FirstName = "������",
                            SecondName = "��������",
                            Surname = "������",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1
                        }
                });

            context.Memberships.AddOrUpdate(new[]
                {
                    new Membership
                        {
                            UserId = 1,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AJLnd2uLQ8CMMt26cIeuYxrFQJ4Do9BpOqRHQ5WIEFWWF5lPNpgKVF8gqeVXx/O8CQ==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        }
                });

            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM webpages_Membership");
            Sql("DELETE FROM UserProfile");
            Sql("DBCC CHECKIDENT (\"UserProfile\", RESEED, 0);");
        }
    }
}
