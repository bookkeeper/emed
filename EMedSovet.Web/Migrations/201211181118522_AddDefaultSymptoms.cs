using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDefaultSymptoms : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Symptoms.AddOrUpdate(new[]
                {
                    new Symptom
                        {
                            Name = "�������",
                            Description = string.Empty
                        },
                    new Symptom
                        {
                            Name = "�������� ��������",
                            Description = string.Empty
                        },
                        new Symptom
                        {
                            Name = "���������� ����������",
                            Description = string.Empty
                        }
                });
            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM Symptoms");
            Sql("DBCC CHECKIDENT (\"Symptoms\", RESEED, 0);");
        }
    }
}
