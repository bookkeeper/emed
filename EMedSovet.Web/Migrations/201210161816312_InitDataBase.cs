namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class InitDataBase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                {
                    UserId = c.Int(nullable: false, identity: true),
                    UserName = c.String(maxLength: 56),
                })
                .PrimaryKey(t => t.UserId);

        }

        public override void Down()
        {
            DropTable("dbo.UserProfile");
        }
    }
}
