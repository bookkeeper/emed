// <auto-generated />
namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddDoctorDegreeManyToOneRelationship : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddDoctorDegreeManyToOneRelationship));
        
        string IMigrationMetadata.Id
        {
            get { return "201212231824075_AddDoctorDegreeManyToOneRelationship"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
