using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddDefaultRoles : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Roles.AddOrUpdate(new[]
                {
                    new Role {RoleName = "Admin"}, 
                    new Role {RoleName = "Doctor"},
                    new Role {RoleName = "Patient"},
                    new Role {RoleName = "Consultant"}
                });


            context.Genders.AddOrUpdate(new[]
                {
                    new Gender {Name = "U"},
                    new Gender {Name = "M"}, new Gender {Name = "F"}
                });



            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM Genders");
            Sql("DBCC CHECKIDENT (\"Genders\", RESEED, 0);");
            Sql("DELETE FROM webpages_Roles");
            Sql("DBCC CHECKIDENT (\"webpages_Roles\", RESEED, 0);");
        }
    }
}
