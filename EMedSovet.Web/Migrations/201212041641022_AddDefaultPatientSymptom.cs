using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultPatientSymptom : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            var patient =
                context.Patients.Include("Symptom").Include("UserProfile").FirstOrDefault(p => p.UserId == 4);
            if (patient == null) return;
            patient.Symptom.Add(context.Symptoms.ToList().FirstOrDefault(s => s.Name.ToLower() == "�������".ToLower()));
            patient.Symptom.Add(context.Symptoms.ToList().FirstOrDefault(
                        s => s.Name.ToLower() == "�������� ��������".ToLower()));
            context.Patients.AddOrUpdate(patient);
            context.SaveChanges();

        }
        
        public override void Down()
        {
            Sql("DELETE FROM SymptomPatients");
        }
    }
}
