using System.Collections.Generic;
using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddDoctorsUsers : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();

            context.UserProfiles.AddOrUpdate(new[]
                {
                    new UserProfile
                        {
                            UserName = "doctor11",
                            Email = "doctor11@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "�������",
                            SecondName = "���������",
                            Surname = "��������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Doctor")
                                }
                        },
                    new UserProfile
                        {
                            UserName = "doctor22",
                            Email = "doctor22@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "����",
                            SecondName = "����������",
                            Surname = "���������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Doctor")
                                }
                        },
                    new UserProfile
                        {
                            UserName = "doctor33",
                            Email = "doctor33@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 1,
                            FirstName = "�����",
                            SecondName = "����������",
                            Surname = "�����",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Doctor")
                                }
                        },
                    new UserProfile
                        {
                            UserName = "doctor44",
                            Email = "doctor44@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 2,
                            FirstName = "��������",
                            SecondName = "������������",
                            Surname = "��������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Doctor")
                                }
                        },
                    new UserProfile
                        {
                            UserName = "doctor55",
                            Email = "doctor55@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 2,
                            FirstName = "�������",
                            SecondName = "���������",
                            Surname = "���������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Doctor")
                                }
                        },
                    new UserProfile
                        {
                            UserName = "doctor66",
                            Email = "doctor66@localhost.com",
                            IsPersonalDataConfirmed = true,
                            GenderId = 2,
                            FirstName = "���������",
                            SecondName = "���������",
                            Surname = "��������",
                            Roles = new List<Role>
                                {
                                    context.Roles.FirstOrDefault(r => r.RoleName == "Doctor")
                                }
                        },


                });

            context.Memberships.AddOrUpdate(new[]
                {
                    new Membership
                        {
                            UserId = 2,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AKUGUb+0orE1gLRAoR9nyx6m2qNahRwT74vMt8BrQxAuNuo6KlN2XXERVqOq6kh6BA==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                        new Membership
                        {
                            UserId = 3,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "ANjfw55ztMuHSmDTMBE7jNDBHR6T71BUQ0307KFhdUMJCBD9YGDwrzm8dPZmXzYqMg==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                        new Membership
                        {
                            UserId = 4,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "APuybUYQJYI+oXocSqhjohEUVj8jAxhRe3LiUQQquoWyRWh59EfbI8S3wcCVLYEQqw==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                        new Membership
                        {
                            UserId = 5,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AGUQRtiD0MZhpOZgcj+Mpg+oouyMdxb4U9H8WvRWS3pjp5zgnmdGL60ArU50dgomPw==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                        new Membership
                        {
                            UserId = 6,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "ADgswrrJmS1UhrXlbj5ekG3IDZoYtNfaNbU3DU7TA+FLkltmbf789qDT73R3WcCinA==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },
                        new Membership
                        {
                            UserId = 7,
                            CreateDate = DateTime.Now,
                            IsConfirmed = true,
                            Password = "AP6CDW1L8vBEFy4/3HWYuUVraLOgHHFy6NahEtlEZ8JKgVzhgA5RLbAM4Ddk2SP4Qw==",
                            PasswordChangedDate = DateTime.Now,
                            PasswordFailuresSinceLastSuccess = 0,
                            PasswordSalt = string.Empty
                        },

                });

            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM webpages_Membership WHERE UserId IN(2,3,4,5,6,7)");
            Sql("DELETE FROM UserProfile  WHERE UserId IN(2,3,4,5,6,7)");
            Sql("DBCC CHECKIDENT (\"UserProfile\", RESEED, 1);");
        }
    }
}
