namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddGenderAndNewUsersColumns : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Genders",
                c => new
                {
                    GenderId = c.Int(nullable: false, identity: true),
                    Name = c.String(maxLength: 1),
                })
                .PrimaryKey(t => t.GenderId);

            AddColumn("dbo.UserProfile", "Email", c => c.String());
            AddColumn("dbo.UserProfile", "IsPersonalDataConfirmed", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfile", "FirstName", c => c.String());
            AddColumn("dbo.UserProfile", "SecondName", c => c.String());
            AddColumn("dbo.UserProfile", "Surname", c => c.String());
            AddColumn("dbo.UserProfile", "Age", c => c.Int());
            AddColumn("dbo.UserProfile", "GenderId", c => c.Int(nullable: false, defaultValue: 1));
            AddForeignKey("dbo.UserProfile", "GenderId", "dbo.Genders", "GenderId", cascadeDelete: true);
            CreateIndex("dbo.UserProfile", "GenderId");
        }

        public override void Down()
        {
            DropIndex("dbo.UserProfile", new[] { "GenderId" });
            DropForeignKey("dbo.UserProfile", "GenderId", "dbo.Genders");
            DropColumn("dbo.UserProfile", "GenderId");
            DropColumn("dbo.UserProfile", "Age");
            DropColumn("dbo.UserProfile", "Surname");
            DropColumn("dbo.UserProfile", "SecondName");
            DropColumn("dbo.UserProfile", "FirstName");
            DropColumn("dbo.UserProfile", "IsPersonalDataConfirmed");
            DropColumn("dbo.UserProfile", "Email");
            DropTable("dbo.Genders");
        }
    }
}
