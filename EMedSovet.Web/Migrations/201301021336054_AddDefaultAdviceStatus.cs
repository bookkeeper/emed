using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultAdviceStatus : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.AdviceStatuses.AddOrUpdate(new[]
                {
                    new AdviceStatus
                        {
                            Name = "�������",
                        },
                    new AdviceStatus
                        {
                            Name = "��������",
                        },
                    new AdviceStatus
                        {
                            Name = "��������",
                        },
                    new AdviceStatus
                        {
                            Name = "���������",
                        }

                });
            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM AdviceStatus");
            Sql("DBCC CHECKIDENT (\"AdviceStatus\", RESEED, 0);");
        }
    }
}
