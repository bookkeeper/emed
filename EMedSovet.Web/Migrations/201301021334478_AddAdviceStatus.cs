namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddAdviceStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdviceStatus",
                c => new
                    {
                        AdviceStatusId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.AdviceStatusId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdviceStatus");
        }
    }
}
