using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDefaultDiseasesArticle : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Diseases.AddOrUpdate(new[]
                {
                    new Disease {Name = "�����", Description = string.Empty, Article = "�����.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "�����", Description = string.Empty, Article = "�����.html"},
                    new Disease
                        {Name = "��������� ���������", Description = string.Empty, Article = "���������_���������.html"}
                    ,
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease
                        {Name = "�������� (������)", Description = string.Empty, Article = "��������_(������).html"},
                    new Disease {Name = "������������", Description = string.Empty, Article = "������������.html"},
                    new Disease {Name = "���� � ������", Description = string.Empty, Article = "����_�_������.html"},
                    new Disease {Name = "���� � ��������", Description = string.Empty, Article = "����_�_��������.html"}
                    ,
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease
                        {
                            Name = "������-���������� ��������",
                            Description = string.Empty,
                            Article = "������-����������_��������.html"
                        },
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease
                        {
                            Name = "������������������ ���������� ������� (����)",
                            Description = string.Empty,
                            Article = "������������������_����������_�������_(����).html"
                        },
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "������������", Description = string.Empty, Article = "������������.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease
                        {Name = "������ �����������", Description = string.Empty, Article = "������_�����������.html"},
                    new Disease {Name = "������������", Description = string.Empty, Article = "������������.html"},
                    new Disease
                        {
                            Name = "���������������� ��������",
                            Description = string.Empty,
                            Article = "����������������_��������.html"
                        },
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease
                        {Name = "������ (���������)", Description = string.Empty, Article = "������_(���������).html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������� ����", Description = string.Empty, Article = "��������_����.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "�����", Description = string.Empty, Article = "�����.html"},
                    new Disease {Name = "�����", Description = string.Empty, Article = "�����.html"},
                    new Disease
                        {
                            Name = "����� ��������������",
                            Description = string.Empty,
                            Article = "�����_��������������.html"
                        },
                    new Disease
                        {
                            Name = "�������������� ����������",
                            Description = string.Empty,
                            Article = "��������������_����������.html"
                        },
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "������������", Description = string.Empty, Article = "������������.html"},
                    new Disease
                        {
                            Name = "�������������� �������",
                            Description = string.Empty,
                            Article = "��������������_�������.html"
                        },
                    new Disease {Name = "���� � ����", Description = string.Empty, Article = "����_�_����.html"},
                    new Disease {Name = "������ �����", Description = string.Empty, Article = "������_�����.html"},
                    new Disease
                        {Name = "����������� �����", Description = string.Empty, Article = "�����������_�����.html"},
                    new Disease
                        {
                            Name = "������������ �����������",
                            Description = string.Empty,
                            Article = "������������_�����������.html"
                        },
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease
                        {Name = "����������� ������", Description = string.Empty, Article = "�����������_������.html"},
                    new Disease {Name = "�����", Description = string.Empty, Article = "�����.html"},
                    new Disease
                        {Name = "����� � ���������", Description = string.Empty, Article = "�����_�_���������.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease {Name = "��������������", Description = string.Empty, Article = "��������������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease
                        {
                            Name = "����� �������: ���������� � ��������",
                            Description = string.Empty,
                            Article = "�����_�������__����������_�_��������.html"
                        },
                    new Disease {Name = "�����������", Description = string.Empty, Article = "�����������.html"},
                    new Disease
                        {
                            Name = "����� � ����������� ��� �������",
                            Description = string.Empty,
                            Article = "�����_�_�����������_���_�������.html"
                        },
                    new Disease {Name = "������������", Description = string.Empty, Article = "������������.html"},
                    new Disease
                        {
                            Name = "�������� �� ���� (������ �������)",
                            Description = string.Empty,
                            Article = "��������_��_����_(������_�������).html"
                        },
                    new Disease
                        {
                            Name = "���������� ���������",
                            Description = string.Empty,
                            Article = "����������_���������.html"
                        },
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease
                        {
                            Name = "����������� ��������������� �����",
                            Description = string.Empty,
                            Article = "�����������_���������������_�����.html"
                        },
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "������ ������", Description = string.Empty, Article = "������_������.html"},
                    new Disease
                        {Name = "������ ����������", Description = string.Empty, Article = "������_����������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "���������������", Description = string.Empty, Article = "���������������.html"}
                    ,
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "������� ������", Description = string.Empty, Article = "�������_������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "����� ����", Description = string.Empty, Article = "�����_����.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "����� �����", Description = string.Empty, Article = "�����_�����.html"},
                    new Disease {Name = "�����������", Description = string.Empty, Article = "�����������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease
                        {
                            Name = "������ (����������� ������)",
                            Description = string.Empty,
                            Article = "������_(�����������_������).html"
                        },
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease
                        {
                            Name = "�������������� �������� ����",
                            Description = string.Empty,
                            Article = "��������������_��������_����.html"
                        },
                    new Disease {Name = "�����������", Description = string.Empty, Article = "�����������.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease
                        {
                            Name = "������� ��������� �����",
                            Description = string.Empty,
                            Article = "�������_���������_�����.html"
                        },
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease
                        {
                            Name = "���������� ����� ������",
                            Description = string.Empty,
                            Article = "����������_�����_������.html"
                        },
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "��������������", Description = string.Empty, Article = "��������������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease
                        {
                            Name = "������� ���������������",
                            Description = string.Empty,
                            Article = "�������_���������������.html"
                        },
                    new Disease
                        {
                            Name = "��� (����������������� �������)",
                            Description = string.Empty,
                            Article = "���_(�����������������_�������).html"
                        },
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "����� ������", Description = string.Empty, Article = "�����_������.html"},
                    new Disease {Name = "������ �����", Description = string.Empty, Article = "������_�����.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "������ �����", Description = string.Empty, Article = "������_�����.html"},
                    new Disease {Name = "��� �����", Description = string.Empty, Article = "���_�����.html"},
                    new Disease
                        {Name = "���������� �������", Description = string.Empty, Article = "����������_�������.html"},
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "�����-��������", Description = string.Empty, Article = "�����-��������.html"},
                    new Disease {Name = "������� �������", Description = string.Empty, Article = "�������_�������.html"}
                    ,
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������� ������", Description = string.Empty, Article = "��������_������.html"}
                    ,
                    new Disease
                        {
                            Name = "������� �������������� ��������",
                            Description = string.Empty,
                            Article = "�������_��������������_��������.html"
                        },
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease
                        {Name = "��������� �������", Description = string.Empty, Article = "���������_�������.html"},
                    new Disease
                        {Name = "��������� ������", Description = string.Empty, Article = "���������_������.html"},
                    new Disease {Name = "������� �� ���", Description = string.Empty, Article = "�������_��_���.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease {Name = "�������������", Description = string.Empty, Article = "�������������.html"},
                    new Disease {Name = "������ �������", Description = string.Empty, Article = "������_�������.html"},
                    new Disease
                        {Name = "��������� �������", Description = string.Empty, Article = "���������_�������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "�������� �����", Description = string.Empty, Article = "��������_�����.html"},
                    new Disease {Name = "���� ����", Description = string.Empty, Article = "����_����.html"},
                    new Disease {Name = "�����������", Description = string.Empty, Article = "�����������.html"},
                    new Disease {Name = "����", Description = string.Empty, Article = "����.html"},
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "������������", Description = string.Empty, Article = "������������.html"},
                    new Disease {Name = "�����", Description = string.Empty, Article = "�����.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease {Name = "�������", Description = string.Empty, Article = "�������.html"},
                    new Disease
                        {
                            Name = "������������ ������ (helicobacter pylori)",
                            Description = string.Empty,
                            Article = "������������_������_(helicobacter_pylori).html"
                        },
                    new Disease {Name = "���������", Description = string.Empty, Article = "���������.html"},
                    new Disease {Name = "����������", Description = string.Empty, Article = "����������.html"},
                    new Disease
                        {
                            Name = "����������� ������������� ������� ������ (����)",
                            Description = string.Empty,
                            Article = "�����������_�������������_�������_������_(����).html"
                        },
                    new Disease {Name = "��������", Description = string.Empty, Article = "��������.html"},
                    new Disease {Name = "������", Description = string.Empty, Article = "������.html"},
                    new Disease
                        {
                            Name = "���������������� ����� (������������)",
                            Description = string.Empty,
                            Article = "����������������_�����_(������������).html"
                        },
                    new Disease {Name = "����� ��������", Description = string.Empty, Article = "�����_��������.html"},
                    new Disease
                        {Name = "���������� ������", Description = string.Empty, Article = "����������_������.html"},
                    new Disease {Name = "�����������", Description = string.Empty, Article = "�����������.html"},
                    new Disease {Name = "�������������", Description = string.Empty, Article = "�������������.html"},
                    new Disease
                        {
                            Name = "����������� ���������� (����������)",
                            Description = string.Empty,
                            Article = "�����������_����������_(����������).html"
                        },
                    new Disease
                        {Name = "������ ����� �����", Description = string.Empty, Article = "������_�����_�����.html"},
                    new Disease
                        {Name = "�������� �������", Description = string.Empty, Article = "��������_�������.html"},

                });



            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM Diseases");
            Sql("DBCC CHECKIDENT (\"Diseases\", RESEED, 0);");
        }
    }
}
