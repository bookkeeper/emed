using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDefaultAdminUserRole : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            var user = context.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == "admin");
            //var membership = context.Memberships.FirstOrDefault(m => m.UserId == user.UserId);
            var role = context.Roles.FirstOrDefault(r => r.RoleName == "Admin");
            if (user != null && role != null)
                user.Roles.Add(role);
            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM webpages_UsersInRoles");
        }
    }
}
