namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddPatientSymptomManyToManyRelationship : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SymptomPatients",
                c => new
                    {
                        Symptom_SymptomId = c.Int(nullable: false),
                        Patient_PatientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Symptom_SymptomId, t.Patient_PatientId })
                .ForeignKey("dbo.Symptoms", t => t.Symptom_SymptomId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_PatientId, cascadeDelete: true)
                .Index(t => t.Symptom_SymptomId)
                .Index(t => t.Patient_PatientId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.SymptomPatients", new[] { "Patient_PatientId" });
            DropIndex("dbo.SymptomPatients", new[] { "Symptom_SymptomId" });
            DropForeignKey("dbo.SymptomPatients", "Patient_PatientId", "dbo.Patients");
            DropForeignKey("dbo.SymptomPatients", "Symptom_SymptomId", "dbo.Symptoms");
            DropTable("dbo.SymptomPatients");
        }
    }
}
