using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddDefaultSpecialties : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Specialties.AddOrUpdate(new[]
                {
                    new Specialty {Name = "������-���������"}, 
                    new Specialty {Name = "��������"}, 
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "����������-���������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "��������-������������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "��������"},

                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "����������"},

                    new Specialty {Name = "��������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "���� ����� ��������"},
                    new Specialty {Name = "���� �� �������� ����������� � ������"},
                    new Specialty {Name = "���� �� ������� ���������"},
                    new Specialty {Name = "���� �� ���������� ��������"},
                    new Specialty {Name = "���� ������ ������"},
                    new Specialty {Name = "���� ���"},
                    new Specialty {Name = "���� �������������� �����������"},
                    
                    new Specialty {Name = "���������������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "���������-�����������"},
                    new Specialty {Name = "���������-������������"},
                    new Specialty {Name = "��������������"},
                    
                    new Specialty {Name = "����������������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "��������"},
                    
                    new Specialty {Name = "������ ����"},
                    
                    new Specialty {Name = "��������������������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "������������"},
                    
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "����������������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "����������"},
                    
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "���"},
                    
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "���������� ��������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "�������"},
                    
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "����������������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "��������"},
                    
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "�������-���������"},
                    new Specialty {Name = "�������-������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "�������-�����������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�����������-������"},
                    
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "�������-����������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "������������ ������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "���������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "��������-��������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "�����������"},
                    
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "����������������"},
                    
                    new Specialty {Name = "���������� ���� �� ������� ����� � ����������"},
                    new Specialty {Name = "���������� ���� �� ������� �������"},
                    new Specialty {Name = "���������� ���� �� ������� �����"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "�������� ����"},
                    new Specialty {Name = "�������� ������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "���������� ������"},
                    new Specialty {Name = "���������� ������������������ �������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "����������-��������"},
                    new Specialty {Name = "����������-��������"},
                    new Specialty {Name = "����������-�������"},
                    new Specialty {Name = "����������-���������"},
                    new Specialty {Name = "����������-��������"},
                    new Specialty {Name = "����������-������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�������-����������� �������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "������������"},
                    
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "�������� ����������"},
                    new Specialty {Name = "����������"},
                    new Specialty {Name = "����������� ������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "��������"},
                    
                    new Specialty {Name = "��� ����"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "������"},
                    
                    new Specialty {Name = "���������� �����������"},
                    new Specialty {Name = "�������������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "�������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "�������������"},
                    
                    new Specialty {Name = "������"},
                    new Specialty {Name = "������ �������"},
                    new Specialty {Name = "������ ������������"},
                    new Specialty {Name = "������ ����������"},
                    new Specialty {Name = "������ �����������"},
                    new Specialty {Name = "������ ��������-�������"},
                    new Specialty {Name = "������-��������"},
                    
                    new Specialty {Name = "��������-������� ������"},

                    new Specialty {Name = "���������"},
                    new Specialty {Name = "��������"},
                    new Specialty {Name = "������������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�����������"},
                    new Specialty {Name = "�����������"}
                });

            context.SaveChanges();
        }

        public override void Down()
        {
            Sql("DELETE FROM Specialties");
            Sql("DBCC CHECKIDENT (\"Specialties\", RESEED, 0);");
        }
    }
}
