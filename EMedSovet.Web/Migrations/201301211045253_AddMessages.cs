namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddMessages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        AnswerId = c.Int(nullable: true),
                        To = c.Int(nullable: true),
                        From = c.Int(nullable: true),
                        Subject = c.String(),
                        Email = c.String(),
                        Text = c.String(),
                        CreatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.UserProfile", t => t.To, cascadeDelete: false)
                .Index(t => t.To)
                .ForeignKey("dbo.UserProfile", t => t.From, cascadeDelete: false)
                .Index(t => t.From)
                .ForeignKey("dbo.Messages", t => t.AnswerId, cascadeDelete: false)
                .Index(t => t.AnswerId);
            
        }
        
        public override void Down()
        {
            //DropIndex("dbo.Messages", new[] { "AnswerId" });
            //DropForeignKey("dbo.Messages", "AnswerId", "dbo.Messages");
            DropIndex("dbo.Messages", new[] { "From" });
            DropForeignKey("dbo.Messages", "From", "dbo.UserProfile");
            DropIndex("dbo.Messages", new[] { "To" });
            DropForeignKey("dbo.Messages", "To", "dbo.UserProfile");
            DropTable("dbo.Messages");
        }
    }
}
