namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddPatiens : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        PatientId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Occupation = c.String(maxLength: 500),
                        Foto = c.String(maxLength: 500),
                        Complains = c.String(),
                        Diagnosis = c.String(),
                        Treatment = c.String(),
                        AssociatedDiseases = c.String(),
                        AssociatedTherapy = c.String(),
                        Surgery = c.String(),
                        Allergy = c.String(),
                        PhysicalExaminationData = c.String(),
                        TimeOfFirstAppearance = c.String(),
                        MoreInfo = c.String(),
                        Survey = c.String(),
                        IsActive = c.Boolean(nullable: true, defaultValue: false),
                    })
                .PrimaryKey(t => t.PatientId)
                .ForeignKey("dbo.UserProfile", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Patients", new[] { "UserId" });
            DropForeignKey("dbo.Patients", "UserId", "dbo.UserProfile");
            DropTable("dbo.Patients");
        }
    }
}
