using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultDegrees : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            context.Degrees.AddOrUpdate(new[]
                {
                    new Degree
                        {
                            Name = "�.�.�., ���������",
                            Priority = 1
                        },
                    new Degree
                        {
                            Name = "�.�.�., ������",
                            Priority = 2
                        },
                    new Degree
                        {
                            Name = "����, ������ ���������",
                            Priority = 3
                        },
                    new Degree
                        {
                            Name = "����",
                            Priority = 4
                        }

                });
            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Degrees");
            Sql("DBCC CHECKIDENT (\"Degrees\", RESEED, 0);");
        }
    }
}
