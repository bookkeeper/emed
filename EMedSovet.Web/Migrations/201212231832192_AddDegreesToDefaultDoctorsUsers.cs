using System.Linq;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddDegreesToDefaultDoctorsUsers : DbMigration
    {
        public override void Up()
        {
            var context = new EMedSovetEntities();
            var doctor11 = context.Doctors.Include("Specialty").FirstOrDefault(d=>d.UserProfile.UserName == "doctor11");
            if (doctor11 != null)
                doctor11.Degree = context.Degrees.FirstOrDefault(d => d.Name == "�.�.�., ���������");
            
            var doctor22 = context.Doctors.Include("Specialty").FirstOrDefault(d => d.UserProfile.UserName == "doctor22");
            if (doctor22 != null)
                doctor22.Degree = context.Degrees.FirstOrDefault(d => d.Name == "�.�.�., ���������");
            
            var doctor33 = context.Doctors.Include("Specialty").FirstOrDefault(d => d.UserProfile.UserName == "doctor33");
            if (doctor33 != null)
                doctor33.Degree = context.Degrees.FirstOrDefault(d => d.Name == "�.�.�., ���������");

            var doctor44 = context.Doctors.Include("Specialty").FirstOrDefault(d => d.UserProfile.UserName == "doctor44");
            if (doctor44 != null)
                doctor44.Degree = context.Degrees.FirstOrDefault(d => d.Name == "�.�.�., ���������");
            
            var doctor55 = context.Doctors.Include("Specialty").FirstOrDefault(d => d.UserProfile.UserName == "doctor55");
            if (doctor55 != null)
                doctor55.Degree = context.Degrees.FirstOrDefault(d => d.Name == "�.�.�., ������");

            var doctor66 = context.Doctors.Include("Specialty").FirstOrDefault(d => d.UserProfile.UserName == "doctor66");
            if (doctor66 != null)
                doctor66.Degree = context.Degrees.FirstOrDefault(d => d.Name == "����, ������ ���������");

            context.SaveChanges();
        }
        
        public override void Down()
        {
            Sql("UPDATE Doctors SET DegreeId = NULL");
        }
    }
}
