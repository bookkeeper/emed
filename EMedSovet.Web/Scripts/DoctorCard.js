﻿$(function () {
    
    $('#consultationDate').watermark('Введите дату');

    $('#consultationDate').datepicker({
        //dateFormat: this.DateFormat,
	    changeMonth: true,
	    changeYear: true,
	    minDate: new Date(),
	    dateFormat: "dd/mm/yy"
	    //onSelect: (onSelectFunction == null ? function () { } : onSelectFunction)
    });

    $('#consultationDate, #consultationHour, #consultationMinute').change(function () {
        alert("Выбрана дата: " + $('#consultationDate').val() + " " + ($('#consultationHour').val() == null || $('#consultationHour').val() == "" ? "0" : $('#consultationHour').val()) + ":" + $('#consultationMinute').val());
    });

});