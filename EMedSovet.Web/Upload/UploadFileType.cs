﻿namespace EMedSovet.Web.Upload
{
    public enum UploadFileType
    {
        Document,
        Photo,
        Music,
        Video
    }
}