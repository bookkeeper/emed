﻿namespace EMedSovet.Web.Upload
{
    public class UploadSettings
    {
        public string UploadOwnerType { get; set; }
        public int UploadOwnerId { get; set; }
        public bool AllowDelete { get; set; }
    }
}