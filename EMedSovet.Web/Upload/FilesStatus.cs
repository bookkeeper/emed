﻿using System;
using System.IO;
using System.Web;

namespace EMedSovet.Web.Upload
{
    public class FilesStatus
    {
        public string HandlerPath = string.Format("{0}{1}", HttpContext.Current.Request.ApplicationPath,
                                                  "/Upload/");

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }

        public FilesStatus() { }

        public FilesStatus(FileInfo fileInfo, string uploadOwnerType, int uploadOwnerId) { SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName, uploadOwnerType, uploadOwnerId); }

        public FilesStatus(string fileName, int fileLength, string fullPath, string uploadOwnerType, int uploadOwnerId) { SetValues(fileName, fileLength, fullPath, uploadOwnerType, uploadOwnerId); }

        private void SetValues(string fileName, int fileLength, string fullPath, string uploadOwnerType, int uploadOwnerId)
        {
            var path = HandlerPath + "UploadHandler.ashx?f=" + fileName + "&UploadOwnerType=" + uploadOwnerType + "&UploadOwnerId=" + uploadOwnerId;
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = path;
            delete_url = path;
            delete_type = "DELETE";

            var ext = Path.GetExtension(fullPath);

            var fileSize = ConvertBytesToMegabytes(new FileInfo(fullPath).Length);
            if (fileSize > 3 || !IsImage(ext))
                thumbnail_url = string.Format("{0}{1}", HttpContext.Current.Request.ApplicationPath,
                                              "/Content/img/generalFile.png");
            else thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
        }

        private bool IsImage(string ext)
        {
            return ext == ".gif" || ext == ".jpg" || ext == ".png";
        }

        private string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }

}