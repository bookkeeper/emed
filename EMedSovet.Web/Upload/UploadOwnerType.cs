﻿namespace EMedSovet.Web.Upload
{
    public enum UploadOwnerType
    {
        Doctor,
        Patient,
        News,
        Article
    }
}