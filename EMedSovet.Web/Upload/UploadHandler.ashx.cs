﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Upload
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler
    {
        private readonly string[] _docExtensions = new[] { ".doc", ".docx", ".xls", ".xlsx" };
        private readonly string[] _photoExtensions = new[] { ".png", ".jpg", ".gif" };
        private readonly string[] _filesFolders = new[] { "Docs", "Photo", "General" };
        private readonly JavaScriptSerializer _js;

        private static readonly Dictionary<String, String> Mtypes = new Dictionary<string, string>
            {
                {".pdf", "application/pdf"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".png", "image/png"},
                {".gif", "image/gif"},
                {".doc", "application/msword"},
                {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}
            };
        private static string MimeType(string filePath)
        {
            var file = new FileInfo(filePath);
            var filetype = file.Extension.ToLower();
            return Mtypes.Keys.Contains(filetype) ? Mtypes[filetype] : string.Empty;
        }
        private static string StorageRoot
        {
            get { return Path.Combine(HttpContext.Current.Server.MapPath("~/Files/")); } //Path should! always end with '/'
        }
        public UploadHandler()
        {
            _js = new JavaScriptSerializer { MaxJsonLength = 41943040 };
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");

            HandleMethod(context);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        // Handle request based on method
        private void HandleMethod(HttpContext context)
        {
            switch (context.Request.HttpMethod)
            {
                case "HEAD":
                case "GET":
                    if (GivenFilename(context)) DeliverFile(context);
                    else ListCurrentFiles(context);
                    break;

                case "POST":
                case "PUT":
                    UploadFile(context);
                    break;

                case "DELETE":
                    DeleteFile(context);
                    break;

                case "OPTIONS":
                    ReturnOptions(context);
                    break;

                default:
                    context.Response.ClearHeaders();
                    context.Response.StatusCode = 405;
                    break;
            }
        }

        private static void ReturnOptions(HttpContext context)
        {
            context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
            context.Response.StatusCode = 200;
        }

        // Delete file from the server
        private void DeleteFile(HttpContext context)
        {
            //var filePath = StorageRoot + context.Request["f"];
            var filePath = GetUploadDir(context.Request["f"]);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                DeleteDocument(context.Request["f"]);
            }
        }

        private void DeleteDocument(string fileName)
        {
            var uploadOwnerType = HttpContext.Current.Request["UploadOwnerType"];
            var uploadOwnerId = Convert.ToInt32(HttpContext.Current.Request["UploadOwnerId"]);
            using (var db = new EMedSovetEntities())
            {
                if (uploadOwnerType == "Doctor")
                {
                    var doctor = db.Doctors.Include("UserProfile").FirstOrDefault(d => d.DoctorId == uploadOwnerId);

                    if (doctor != null)
                    {
                        var document =
                        db.Documents.Include("UserProfiles").FirstOrDefault(doc => doc.FileName == fileName && doc.OwnerId == doctor.DoctorId);
                        if (document != null)
                        {
                            db.Documents.Remove(document);
                            //db.SaveChanges();
                        }
                    }
                }
                if (uploadOwnerType == "Patient")
                {
                    var patient = db.Patients.Include("UserProfile").FirstOrDefault(p => p.PatientId == uploadOwnerId);

                    if (patient != null)
                    {
                        var document =
                        db.Documents.Include("UserProfiles").FirstOrDefault(doc => doc.FileName == fileName && doc.OwnerId == patient.PatientId);
                        if (document != null)
                        {
                            db.Documents.Remove(document);
                            
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        // Upload file to the server
        private void UploadFile(HttpContext context)
        {
            var statuses = new List<FilesStatus>();
            var headers = context.Request.Headers;

            if (string.IsNullOrEmpty(headers["X-File-Name"]))
            {
                UploadWholeFile(context, statuses);
            }
            else
            {
                UploadPartialFile(headers["X-File-Name"], context, statuses);
            }

            WriteJsonIframeSafe(context, statuses);
        }

        // Upload partial file
        private void UploadPartialFile(string fileName, HttpContext context, List<FilesStatus> statuses)
        {

            if (context.Request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
            var inputStream = context.Request.Files[0].InputStream;
            //var fullName = StorageRoot + Path.GetFileName(fileName);
            var fullName = GetUploadDir(fileName);

            using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
            {
                var buffer = new byte[1024];

                var l = inputStream.Read(buffer, 0, 1024);
                while (l > 0)
                {
                    fs.Write(buffer, 0, l);
                    l = inputStream.Read(buffer, 0, 1024);
                }
                fs.Flush();
                fs.Close();
            }
            var uploadOwnerType = HttpContext.Current.Request["UploadOwnerType"];
            var uploadOwnerId = Convert.ToInt32(HttpContext.Current.Request["UploadOwnerId"]);
            statuses.Add(new FilesStatus(new FileInfo(fullName), uploadOwnerType, uploadOwnerId));
        }

        private string GetStoreFolderNameByExtension(string fileName)
        {
            var fileInfo = new FileInfo(fileName);
            if (_docExtensions.Contains(fileInfo.Extension)) return "Docs";
            if (_photoExtensions.Contains(fileInfo.Extension)) return "Photo";
            return "General";
        }

        private string GetUploadDir(string fileName)
        {
            var uploadOwnerType = HttpContext.Current.Request["UploadOwnerType"];
            var uploadOwnerId = HttpContext.Current.Request["UploadOwnerId"];
            var storeFolderNameByExtension = GetStoreFolderNameByExtension(fileName);
            var userUploadDir = string.Format("{0}\\{1}\\{2}\\{3}", StorageRoot, uploadOwnerType, uploadOwnerId, storeFolderNameByExtension);
            if (!Directory.Exists(userUploadDir)) Directory.CreateDirectory(userUploadDir);
            return string.Format("{0}\\{1}", userUploadDir, Path.GetFileName(fileName));
        }

        // Upload entire file
        private void UploadWholeFile(HttpContext context, List<FilesStatus> statuses)
        {


            for (var i = 0; i < context.Request.Files.Count; i++)
            {
                var file = context.Request.Files[i];

                //var fullPath = StorageRoot + Path.GetFileName(file.FileName);
                var fullPath = GetUploadDir(file.FileName);

                file.SaveAs(fullPath);

                var fullName = Path.GetFileName(file.FileName);
                var uploadOwnerType = HttpContext.Current.Request["UploadOwnerType"];
                var uploadOwnerId = Convert.ToInt32(HttpContext.Current.Request["UploadOwnerId"]);
                statuses.Add(new FilesStatus(fullName, file.ContentLength, fullPath, uploadOwnerType, uploadOwnerId));
                AddDocument(file.FileName);
            }
        }

        private void AddDocument(string fileName)
        {

            var uploadOwnerType = HttpContext.Current.Request["UploadOwnerType"];
            var uploadOwnerId = Convert.ToInt32(HttpContext.Current.Request["UploadOwnerId"]);
            using (var db = new EMedSovetEntities())
            {
                if (uploadOwnerType == "Doctor")
                {
                    var doctor = db.Doctors.Include("UserProfile").FirstOrDefault(d => d.DoctorId == uploadOwnerId);
                    if (doctor != null)
                    {
                        var fileInfo = new FileInfo(fileName);
                        var document = new Document
                            {
                                OwnerId = doctor.DoctorId,
                                FileName = fileName,
                                Extension = fileInfo.Extension,
                                MimeType = MimeType(fileName),

                            };
                        document.UserProfiles.Add(doctor.UserProfile);
                        db.Documents.Add(document);
                        //db.SaveChanges();
                    }

                }

                if (uploadOwnerType == "Patient")
                {
                    var patient = db.Patients.Include("UserProfile").FirstOrDefault(p => p.PatientId == uploadOwnerId);
                    if (patient != null)
                    {
                        var fileInfo = new FileInfo(fileName);
                        var document = new Document
                        {
                            OwnerId = patient.PatientId,
                            FileName = fileName,
                            Extension = fileInfo.Extension,
                            MimeType = MimeType(fileName),

                        };
                        document.UserProfiles.Add(patient.UserProfile);
                        db.Documents.Add(document);
                        
                    }

                }
                db.SaveChanges();
            }

        }

        private void WriteJsonIframeSafe(HttpContext context, List<FilesStatus> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
            try
            {
                context.Response.ContentType = context.Request["HTTP_ACCEPT"].Contains("application/json")
                                                   ? "application/json"
                                                   : "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }

            var jsonObj = _js.Serialize(statuses.ToArray());
            context.Response.Write(jsonObj);
        }

        private static bool GivenFilename(HttpContext context)
        {
            return !string.IsNullOrEmpty(context.Request["f"]);
        }

        private void DeliverFile(HttpContext context)
        {
            var filename = context.Request["f"];
            //var filePath = StorageRoot + filename;
            var filePath = GetUploadDir(context.Request["f"]);

            if (File.Exists(filePath))
            {
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                context.Response.ContentType = "application/octet-stream";
                context.Response.ClearContent();
                context.Response.WriteFile(filePath);
            }
            else
                context.Response.StatusCode = 404;
        }

        private void ListCurrentFiles(HttpContext context)
        {
            //var files =
            //    new DirectoryInfo(StorageRoot)
            //        .GetFiles("*", SearchOption.TopDirectoryOnly)
            //        .Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden))
            //        .Select(f => new FilesStatus(f))
            //        .ToArray();
            var uploadOwnerType = HttpContext.Current.Request["UploadOwnerType"];
            var uploadOwnerId = Convert.ToInt32(HttpContext.Current.Request["UploadOwnerId"]);
            var currentUserUploadDir = string.Format("{0}\\{1}\\{2}", StorageRoot, uploadOwnerType, uploadOwnerId);

            var files = new List<FilesStatus>();
            foreach (var filesFolder in _filesFolders)
            {
                var folderDir = string.Format("{0}\\{1}", currentUserUploadDir, filesFolder);
                if (!Directory.Exists(folderDir)) continue;
                var filesInFolder =
                new DirectoryInfo(folderDir)
                    .GetFiles("*", SearchOption.TopDirectoryOnly)
                    .Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden))
                    .Select(f => new FilesStatus(f, uploadOwnerType, uploadOwnerId));
                files.AddRange(filesInFolder);
            }
            var jsonObj = _js.Serialize(files.ToArray());
            context.Response.AddHeader("Content-Disposition", "inline; filename=\"files.json\"");
            context.Response.Write(jsonObj);
            context.Response.ContentType = "application/json";
        }
    }
}