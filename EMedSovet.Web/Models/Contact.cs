﻿using System.ComponentModel.DataAnnotations;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Contact
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Name", ResourceType = typeof(Resource.Contact))]
        [StringLength(100, ErrorMessageResourceName = "StringLengthPasswordError", ErrorMessageResourceType = typeof(Resources), MinimumLength = 4)]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", ErrorMessageResourceName = "EmailValidError", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email", ResourceType = typeof(Resource.Contact))]
        [StringLength(100, ErrorMessageResourceName = "StringLengthPasswordError", ErrorMessageResourceType = typeof(Resources), MinimumLength = 4)]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Subject", ResourceType = typeof(Resource.Contact))]
        [StringLength(100, ErrorMessageResourceName = "StringLengthPasswordError", ErrorMessageResourceType = typeof(Resources), MinimumLength = 4)]
        public string Subject { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Text", ResourceType = typeof(Resource.Contact))]
        public string Text { get; set; }

        [Display(Name = "From", ResourceType = typeof(Resource.Contact))]
        public int? From { get; set; }
    }
}