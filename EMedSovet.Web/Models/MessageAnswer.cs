﻿using System.ComponentModel.DataAnnotations;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class MessageAnswer
    {

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Text", ResourceType = typeof(Resource.Contact))]
        public string Text { get; set; }

        [Display(Name = "To", ResourceType = typeof(Resource.Contact))]
        public int? MessageId { get; set; }
    }
}