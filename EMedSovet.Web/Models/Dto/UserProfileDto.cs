﻿namespace EMedSovet.Web.Models.Dto
{
    public class UserProfileDto
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public int GenderId { get; set; }
    }
}