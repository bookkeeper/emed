﻿namespace EMedSovet.Web.Models.Dto
{
    public class DoctorDto
    {
        public string Specialties { get; set; }
    }
}