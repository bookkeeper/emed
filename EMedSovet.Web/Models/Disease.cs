﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Disease
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DiseaseId { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [MaxLength(250)]
        public string Name { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string Description { get; set; }
        [Display(Name = "Article", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        public string Article { get; set; }
    }
}