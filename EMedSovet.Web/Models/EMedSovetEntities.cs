﻿using System.Data.Entity;

namespace EMedSovet.Web.Models
{
    public class EMedSovetEntities : DbContext
    {
        public EMedSovetEntities()
            : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Membership>()
            //    .HasMany<Role>(r => r.Roles)
            //    .WithMany(u => u.Members)
            //    .Map(m =>
            //    {
            //        m.ToTable("webpages_UsersInRoles");
            //        m.MapLeftKey("UserId");
            //        m.MapRightKey("RoleId");
            //    });
            modelBuilder.Entity<UserProfile>()
                .HasMany<Role>(r => r.Roles)
                .WithMany(u => u.UserProfiles)
                .Map(m =>
                {
                    m.ToTable("webpages_UsersInRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });
            modelBuilder.Entity<Patient>().Property(p => p.Height).HasPrecision(10, 2);
            modelBuilder.Entity<Patient>().Property(p => p.Weight).HasPrecision(10, 2);
        }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<OAuthMembership> OAuthMemberships { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Specialty> Specialties { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Disease> Diseases { get; set; }
        public DbSet<Symptom> Symptoms { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<AdviceStatus> AdviceStatuses { get; set; }
        public DbSet<Advice> Advices { get; set; }
        public DbSet<Message> Messages { get; set; }

    }
}