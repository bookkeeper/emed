﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Patient
    {
        public Patient()
        {
            Symptom = new HashSet<Symptom>();
            Advices = new HashSet<Advice>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PatientId { get; set; }
        [Required(ErrorMessageResourceName = "RequiredUserNameError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public int UserId { get; set; }
        [Display(Name = "Height", ResourceType = typeof(Resources))]
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        [Range(0, 300.00, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "HeightRangeErrorMessage")]
        public decimal Height { get; set; }
        [Display(Name = "Weight", ResourceType = typeof(Resources))]
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        [Range(0, 500.00, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "WeightRangeErrorMessage")]
        public decimal Weight { get; set; }
        [Display(Name = "Foto", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        public string Foto { get; set; }
        [Display(Name = "Occupation", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        [AllowHtml]
        public string Occupation { get; set; }
        [Display(Name = "Complains", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Complains { get; set; }
        [Display(Name = "Diagnosis", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Diagnosis { get; set; }
        [Display(Name = "Treatment", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Treatment { get; set; }

        [Display(Name = "AssociatedDiseases", ResourceType = typeof(Resources))]
        public string AssociatedDiseases { get; set; }

        [Display(Name = "AssociatedTherapy", ResourceType = typeof(Resources))]
        public string AssociatedTherapy { get; set; }

        [Display(Name = "Surgery", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Surgery { get; set; }

        [Display(Name = "Allergy", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Allergy { get; set; }

        [Display(Name = "PhysicalExaminationData", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string PhysicalExaminationData { get; set; }

        [Display(Name = "TimeOfFirstAppearance", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string TimeOfFirstAppearance { get; set; }

        [Display(Name = "MoreInfo", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string MoreInfo { get; set; }

        [Display(Name = "Survey", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Survey { get; set; }

        [ForeignKey("UserId")]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public UserProfile UserProfile { get; set; }

        [Display(Name = "IsActive", ResourceType = typeof(Resources))]
        public bool? IsActive { get; set; }

        public ICollection<Symptom> Symptom { get; set; }
        public ICollection<Advice> Advices { get; set; }
    }
}