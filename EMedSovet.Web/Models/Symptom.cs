﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Symptom
    {
        public Symptom()
        {
            Patient = new HashSet<Patient>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SymptomId { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        public string Name { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resources))]
        public string Description { get; set; }

        public ICollection<Patient> Patient { get; set; }
    }
}