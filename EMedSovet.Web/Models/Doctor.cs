using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    using System.Collections.Generic;
    
    public class Doctor
    {
        public Doctor()
        {
            Specialty = new HashSet<Specialty>();
            Advices = new HashSet<Advice>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DoctorId { get; set; }
        //[Display(Name = "Degree", ResourceType = typeof(Resources))]
        //[MaxLength(500)]
        //public string Degree { get; set; }
        [Display(Name = "ConsultationArea", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        [AllowHtml]
        public string ConsultationArea { get; set; }
        [Display(Name = "Education", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        [AllowHtml]
        public string Education { get; set; }

        [Display(Name = "Degree", ResourceType = typeof(Resources))]
        public int? DegreeId { get; set; }
        
        [Display(Name = "Resume", ResourceType = typeof(Resources))]
        [MaxLength(1000)]
        [AllowHtml]
        public string Resume { get; set; }

        [Display(Name = "Experience", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Experience { get; set; }
        
        [Display(Name = "Summary", ResourceType = typeof(Resources))]
        [AllowHtml]
        public string Summary { get; set; }
        [Required(ErrorMessageResourceName = "RequiredUserNameError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public int UserId { get; set; }
        [Display(Name = "IsConfirmed", ResourceType = typeof(Resources))]
        public bool? IsConfirmed { get; set; }
        [Display(Name = "Foto", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        public string Foto { get; set; }
        [Display(Name = "DocScan", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        public string DocScan { get; set; }

        [Display(Name = "IsActive", ResourceType = typeof(Resources))]
        public bool? IsActive { get; set; }

        [Display(Name = "Degree", ResourceType = typeof(Resources))]
        [ForeignKey("DegreeId")]
        public Degree Degree { get; set; }

        [ForeignKey("UserId")]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public UserProfile UserProfile { get; set; }

        public ICollection<Specialty> Specialty { get; set; }
        public ICollection<Advice> Advices { get; set; }
    }
}
