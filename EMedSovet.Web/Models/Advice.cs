﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Advice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdviceId { get; set; }
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        public int DoctorId { get; set; }
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        public int PatientId { get; set; }

        public int AdviceStatusId { get; set; }

        [Display(Name = "Created", ResourceType = typeof(Resources))]
        public DateTime? CreatedAt { get; set; }
        [Display(Name = "Completed", ResourceType = typeof(Resources))]
        public DateTime? CompletedAt { get; set; }

        [ForeignKey("DoctorId")]
        public Doctor Doctor { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }

        [ForeignKey("AdviceStatusId")]
        public AdviceStatus AdviceStatus { get; set; }

    }
}