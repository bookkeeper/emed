﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Degree
    {
        public Degree()
        {
            this.Doctor = new HashSet<Doctor>();
        } 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DegreeId { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [MaxLength(250)]
        public string Name { get; set; }

        [Display(Name = "Priority", ResourceType = typeof(Resources))]
        public int? Priority { get; set; }


        public virtual ICollection<Doctor> Doctor { get; set; }
    }
}