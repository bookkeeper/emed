﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace EMedSovet.Web.Models
{

    public class Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }
        public int? AnswerId { get; set; }
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "To", ResourceType = typeof(Resource.Message))]
        public int? To { get; set; }
        [Display(Name = "From", ResourceType = typeof(Resource.Message))]
        public int? From { get; set; }
        [Display(Name = "Subject", ResourceType = typeof(Resource.Message))]
        public string Subject { get; set; }
        [Display(Name = "Email", ResourceType = typeof(Resource.Message))]
        public string Email { get; set; }
        [Display(Name = "Text", ResourceType = typeof(Resource.Message))]
        [AllowHtml]
        public string Text { get; set; }
        [Display(Name = "Created", ResourceType = typeof(Resource.Message))]
        public DateTime? CreatedAt { get; set; }

        [ForeignKey("To")]
        public UserProfile UserProfileTo { get; set; }

        [ForeignKey("From")]
        public UserProfile UserProfileFrom { get; set; }

        //[ForeignKey("AnswerId")]
        //public Message MessageAnswer { get; set; }

        [ForeignKey("AnswerId")]
        public virtual ICollection<Message> MessagesAnswer { get; set; }

        public bool IsLeaf
        {
            get
            {
                return MessagesAnswer.Count == 0;
            }
        }

        public bool IsBranch
        {
            get
            {
                return AnswerId.HasValue && MessagesAnswer.Count == 0;
            }
        }

        public bool IsRoot
        {
            get
            {
                return !AnswerId.HasValue;
            }
        }

    }
}