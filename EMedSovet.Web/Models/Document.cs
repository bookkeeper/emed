﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Document
    {
        public Document()
        {
            UserProfiles = new HashSet<UserProfile>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocumentId { get; set; }

        [Required(ErrorMessageResourceName = "RequiredUserNameError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public int OwnerId { get; set; }

        [Display(Name = "FileName", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "RequiredFileNameError", ErrorMessageResourceType = typeof(Resources))]
        public string FileName { get; set; }

        [Display(Name = "Extension", ResourceType = typeof(Resources))]
        public string Extension { get; set; }

        [Display(Name = "MimeType", ResourceType = typeof(Resources))]
        public string MimeType { get; set; }

        public ICollection<UserProfile> UserProfiles { get; set; }
    }
}