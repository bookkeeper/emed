﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    //public class UsersContext : DbContext
    //{
    //    public UsersContext()
    //        : base("DefaultConnection")
    //    {
    //    }

    //    public DbSet<UserProfile> UserProfiles { get; set; }
    //}

    //[Table("UserProfile")]
    //public class UserProfile
    //{
    //    //public UserProfile()
    //    //{
    //    //    Roles = new HashSet<webpages_Roles>();
    //    //}

    //    [Key]
    //    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    //    public int UserId { get; set; }
    //    public string UserName { get; set; }
    //    public string Email { get; set; }
    //    public bool IsPersonalDataConfirmed { get; set; }
    //    public string FirstName { get; set; }
    //    public string SecondName { get; set; }
    //    public string Surname { get; set; }
    //    public int? Age { get; set; }
    //    public int? RoleId { get; set; }
    //    //public ICollection<webpages_Roles> Roles { get; set; }

       
        
    //}

    public class RegisterExternalLoginModel
    {
        [Required(ErrorMessageResourceName = "RequiredUserNameError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceName = "TermsError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "TermsAccept", ResourceType = typeof(Resources))]
        public bool IsPersonalDataConfirmed { get; set; }

        [Required(ErrorMessageResourceName = "RoleError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Role", ResourceType = typeof(Resources))]
        public string Role { get; set; } 

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessageResourceName = "RequiredUserNameError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "UserName", ResourceType = typeof(Resources))]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceName = "RequiredPassword", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(100, ErrorMessageResourceName = "StringLengthPasswordError", ErrorMessageResourceType = typeof(Resources), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Resources))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources))]
        [Compare("Password", ErrorMessageResourceName = "ConfirmPasswordError", ErrorMessageResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceName = "RequiredEmail", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", ErrorMessageResourceName = "EmailValidError", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email", ResourceType = typeof(Resources))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "TermsError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "TermsAccept", ResourceType = typeof(Resources))]
        public bool IsPersonalDataConfirmed { get; set; }

        [Required(ErrorMessageResourceName = "RoleError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Role", ResourceType = typeof(Resources))]
        public string Role { get; set; } 
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
