﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EMedSovet.Web.Properties;

namespace EMedSovet.Web.Models
{
    public class Specialty
    {
        public Specialty()
        {
            Doctor = new HashSet<Doctor>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SpecialtyId { get; set; }
        [Required(ErrorMessageResourceName = "RequiredSpecialtyError", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "Specialty", ResourceType = typeof(Resources))]
        [MaxLength(500)]
        public string Name { get; set; }

        public ICollection<Doctor> Doctor { get; set; }
    
    }
}