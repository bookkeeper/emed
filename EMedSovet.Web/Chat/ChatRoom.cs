﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace EMedSovet.Web.Chat
{
    /// <summary>
    /// Each chat room contains a hashtable of users and a list of messages
    /// </summary>
    public class ChatRoom : IDisposable
    {
        #region Members

        private readonly Dictionary<string, ChatUser> _roomUsers;
        private readonly int _userChatRoomSessionTimeout;
        public List<ChatMessage> Messages = null;
        public string RoomID;

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Messages.Clear();
            RoomID = string.Empty;
            foreach (var key in _roomUsers.Keys)
            {
                _roomUsers[key].Dispose();
            }
        }

        #endregion

        #region Constructors

        public ChatRoom(string roomID)
        {
            Messages = new List<ChatMessage>();
            RoomID = roomID;
            _userChatRoomSessionTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["UserChatRoomSessionTimeout"]);
            _roomUsers =
                new Dictionary<string, ChatUser>(Convert.ToInt32(ConfigurationManager.AppSettings["ChatRoomMaxUsers"]));
        }

        #endregion

        #region Operations Join,Send,Leave

        /// <summary>
        /// Remove user from room
        /// </summary>
        /// <param name="userId"></param>
        public void LeaveRoom(string userId)
        {
            lock (this)
            {
                //deactivate user 
                var user = GetUser(userId);
                if (user == null)
                    return;
                user.IsActive = false;
                user.LastSeen = DateTime.Now;
                _roomUsers.Remove(userId);

                //Add leaving message
                var msg = new ChatMessage(user, string.Empty, ChatMessageType.Left);
                AddMsg(msg);

                if (IsEmpty()) ChatEngine.DeleteRoom(RoomID);
            }
        }

        /// <summary>
        /// Check if user is empty
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return !_roomUsers.Any();
        }

        /// <summary>
        /// Add new message
        /// </summary>
        /// <param name="msg"></param>
        private void AddMsg(ChatMessage msg)
        {
            Messages.Add(msg);
        }


        /// <summary>
        /// Create new user, add new user to room, send all messages to user
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="userName"></param>
        /// <param name="avatarLink"></param>
        /// <returns></returns>
        public string JoinRoom(string userID, string userName, string avatarLink)
        {
            lock (this)
            {
                //activate user 
                var user = new ChatUser(userID, userName, avatarLink)
                    {
                        IsActive = true,
                        UserName = userName,
                        LastSeen = DateTime.Now
                    };
                if (!_roomUsers.ContainsKey(userID))
                {
                    //Add join message
                    var msg = new ChatMessage(user, "Joined", ChatMessageType.Join);
                    AddMsg(msg);
                    //Get all the messages to the user
                    int lastMsgID;
                    var previousMessages =
                        GetMessagesSince(-1, out lastMsgID);
                    user.LastMessageReceived = lastMsgID;
                    //return the messages to the user
                    var str = GenerateMessagesString(previousMessages);
                    _roomUsers.Add(userID, user);
                    return str;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Send message, retrive all last messages
        /// </summary>
        /// <param name="strMsg"></param>
        /// <param name="senderID"></param>
        /// <returns></returns>
        public string SendMessage(string strMsg, string senderID)
        {
            var user = GetUser(senderID);
            var msg = new ChatMessage(user, strMsg, ChatMessageType.Message);
            user.LastSeen = DateTime.Now;
            ExpireUsers(_userChatRoomSessionTimeout);
            AddMsg(msg);
            int lastMsgID;
            var previousMsgs = GetMessagesSince
                (user.LastMessageReceived, out lastMsgID);
            if (lastMsgID != -1)
                user.LastMessageReceived = lastMsgID;
            var res = GenerateMessagesString(previousMsgs);
            return res;
        }

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="senderID"></param>
        /// <returns></returns>
        public  ChatUser GetUser(string senderID)
        {
            return _roomUsers.FirstOrDefault(u => u.Value.UserID == senderID).Value;
        }

        /// <summary>
        /// Get room usernames
        /// </summary>
        /// <returns></returns>
        public List<string> GetRoomUsersNames()
        {
            return _roomUsers.Select(u => u.Value.UserName).ToList();
        }
        


        /// <summary>
        /// Removes the users that hasn't sent any message during the last window seconds
        /// </summary>
        /// <param name="window">time in secondes</param>
        public void ExpireUsers(int window)
        {
            lock (this)
            {
                foreach (var key in _roomUsers.Keys)
                {
                    var usr = _roomUsers[key];
                    lock (usr)
                    {
                        if (usr.LastSeen == DateTime.MinValue) continue;
                        var span = DateTime.Now - usr.LastSeen;
                        if (span.TotalSeconds > window && usr.IsActive)
                        {
                            LeaveRoom(usr.UserID);
                        }
                    }
                }
            }
        }

        #endregion

         
        /// <summary>
        /// Returns all the messages sent since the last message the user received
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string UpdateUser(string userID)
        {
            var user = GetUser(userID);
            if (user == null) return string.Empty;
            user.LastSeen = DateTime.Now;
            ExpireUsers(_userChatRoomSessionTimeout);
            int lastMsgID;
            var previousMsgs = GetMessagesSince
                (user.LastMessageReceived, out lastMsgID);
            if (lastMsgID != -1)
                user.LastMessageReceived = lastMsgID;
            var res = GenerateMessagesString(previousMsgs);
            return res;
        }

        /// <summary>
        /// Generate messages view
        /// </summary>
        /// <param name="previousMsgs"></param>
        /// <returns></returns>
        private static string GenerateMessagesString(IEnumerable<ChatMessage> previousMsgs)
        {
            var messages = new StringBuilder();
            foreach (var previousMsg in previousMsgs)
            {
                messages.AppendFormat("{0}", previousMsg);
            }

            return messages.ToString();
        }

        
        /// <summary>
        /// Returns a list that contains all messages sent after the message with id=msgid
        /// </summary>
        /// <param name="msgid">The id of the message after which all the message will be retuned</param>
        /// <param name="lastMsgID">The id of the last message returned</param>
        /// <returns></returns>
        public List<ChatMessage> GetMessagesSince(int msgid, out int lastMsgID)
        {
            lock (Messages)
            {
                if ((Messages.Count) <= (msgid + 1))
                    lastMsgID = -1;
                else
                    lastMsgID = Messages.Count - 1;
                return Messages.GetRange(msgid + 1, Messages.Count - (msgid + 1));
            }
        }
    }
}