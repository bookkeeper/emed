﻿namespace EMedSovet.Web.Chat
{
    /// <summary>
    /// Each message that appears in the chat room
    /// </summary>
    public class ChatMessage
    {
        #region Members

        public string Text;
        public ChatMessageType Type;
        public ChatUser User;
        public string AdminsUser = Resource.Chat.AdminsUser;

        #endregion

        #region Constructors

        public ChatMessage(ChatUser user, string text, ChatMessageType type)
        {
            User = user;
            Text = text;
            Type = type;
        }

        public ChatMessage(ChatUser user, ChatMessageType type)
            : this(user, string.Empty, type)
        {
        }

        public ChatMessage(ChatMessageType type) : this(null, string.Empty, type)
        {
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            switch (Type)
            {
                case ChatMessageType.Message:
                    return string.Format("<p>{0}: {1}</p>", GetName(), Text);
                case ChatMessageType.Join:
                    return string.Format("<p>{0} {1}</p>", GetName(), Resource.Chat.JoinMessage);
                case ChatMessageType.Left:
                    return string.Format("<p>{0} {1}</p>", GetName(), Resource.Chat.LeaveMessage);
            }
            return string.Empty;
        }
        public string GetName()
        {
//            if (User == AdminsUser)
//            {
//                return string.Format(@"
//                            <span class=""bold"">
//                                {{{{Avatar}}}}
//                            </span>&nbsp;<span class=""bold"">{0}:</span>", User);
//            }
//            return string.Format("<span class=\"bold\">{0}:</span>", User);

            return string.Format(@"
                            <span class=""bold"">
                                <img src=""{1}"" class=""inline"" style=""width: 50px; height: 50px;"" />
                            </span>&nbsp;<span class=""bold"">{0}:</span>", User.UserName, User.AvatarLink);
        }
        #endregion
    }
}