﻿using System;

namespace EMedSovet.Web.Chat
{
    /// <summary>
    /// This class represents the user that will join the chat room
    /// </summary>
    public class ChatUser : IDisposable
    {
        #region Members

        public bool IsActive;
        public int LastMessageReceived;
        public DateTime LastSeen;
        public string UserID;
        public string UserName;
        public string AvatarLink;
        #endregion

        #region Constructors

        public ChatUser(string id, string userName, string avatarLink)
        {
            UserID = id;
            IsActive = false;
            LastSeen = DateTime.MinValue;
            UserName = userName;
            LastMessageReceived = 0;
            AvatarLink = avatarLink;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            UserID = string.Empty;
            IsActive = false;
            LastSeen = DateTime.MinValue;
            UserName = string.Empty;
            LastMessageReceived = 0;
        }

        #endregion
    }
}