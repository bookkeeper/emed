﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace EMedSovet.Web.Chat
{
    /// <summary>
    /// The ChatEngine class acts as the container for chat rooms
    /// </summary>
    public static class ChatEngine
    {
        #region Members
        private static readonly Dictionary<string, ChatRoom> Rooms =
            new Dictionary<string, ChatRoom>(Convert.ToInt32(ConfigurationManager.AppSettings["MaxChatRooms"]));

        private static readonly int UserChatRoomSessionTimeout =
            Convert.ToInt32(ConfigurationManager.AppSettings["UserChatRoomSessionTimeout"]);
        #endregion

        #region Methods

        /// <summary>
        /// Cleans all the chat rooms. Deletes the empty chat rooms
        /// </summary>
        /// <param name="state"></param>
        public static void CleanChatRooms(object state)
        {
            lock (Rooms)
            {
                foreach (var key in Rooms.Keys)
                {
                    var room = Rooms[key];
                    room.ExpireUsers(UserChatRoomSessionTimeout);
                    if (!room.IsEmpty()) continue;
                    room.Dispose();
                    Rooms.Remove(key);
                }
            }
        }

        /// <summary>
        /// Returns the chat room or create a new one if nothing exists
        /// </summary>
        /// <param name="roomID"></param>
        /// <returns></returns>
        public static ChatRoom GetRoom(string roomID)
        {
            ChatRoom room;
            lock (Rooms)
            {
                if (Rooms.ContainsKey(roomID))
                    room = Rooms[roomID];
                else
                {
                    room = new ChatRoom(roomID);
                    Rooms.Add(roomID, room);
                }
            }
            return room;
        }

    
        /// <summary>
        /// Delete room
        /// </summary>
        /// <param name="roomID"></param>
        public static void DeleteRoom(string roomID)
        {
            if (!Rooms.ContainsKey(roomID))
                return;
            lock (Rooms)
            {
                var room = Rooms[roomID];
                room.Dispose();
                Rooms.Remove(roomID);
            }
        }
        #endregion
    }
}