﻿namespace EMedSovet.Web.Chat
{
    public enum ChatMessageType
    {
        Message, 
        Start, 
        Join, 
        Left, 
        Action
    }
}