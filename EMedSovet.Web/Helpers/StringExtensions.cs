﻿using System;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;

namespace EMedSovet.Web.Helpers
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns part of a string up to the specified number of characters, while maintaining full words
        /// </summary>
        /// <param name="s"></param>
        /// <param name="length">Maximum characters to be returned</param>
        /// <returns>String</returns>
        public static string Chop(this string s, int length)
        {
            //if (String.IsNullOrEmpty(s))
            //    throw new ArgumentNullException(s);
            if (string.IsNullOrEmpty(s))
                return string.Empty;
            var text = Regex.Replace(s, @"\s*<.*?>\s*", string.Empty, RegexOptions.Singleline);
            var words = text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (words[0].Length > length)
                return words[0];
            var sb = new StringBuilder();

            foreach (var word in words)
            {
                if ((sb + word).Length > length)
                    return string.Format("{0}...", sb.ToString().TrimEnd(' '));
                sb.Append(word + " ");
            }
            return string.Format("{0}...", sb.ToString().TrimEnd(' '));
        }

        /// <summary>
        /// Replace image src for news by id 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="path">News id</param>
        /// <returns>String</returns>
        public static string ImgPath(this string s, string path)
        {
            if (string.IsNullOrEmpty(s))
                throw new ArgumentNullException(s);
            var text = Regex.Replace(s, @"(?<=<img\s+[^>]*?src=(?<q>['""]))(?<url>.+?)(?=\k<q>)", m => path + m.Value);
            return string.Format("{0}", text);
        }
        /// <summary>
        /// Replace image src
        /// </summary>
        /// <param name="s"></param>
        /// <returns>String</returns>
        public static string ImgFileName(this string s)
        {
            if (string.IsNullOrEmpty(s))
                throw new ArgumentNullException(s);
            var myRegex = new Regex(@"(?<=<img\s+[^>]*?src=(?<q>['""]))(?<url>.+?)(?=\k<q>)");
            //var text = Regex.Replace(s, @"(?<=<img\s+[^>]*?src=(?<q>['""]))(?<url>.+?)(?=\k<q>)", m => m.Value);
            foreach (Match myMatch in myRegex.Matches(s))
            {
                if (myMatch.Success)
                {
                    var pathSplitted = myMatch.Value.Split(new[]{'/'});
                    var fileName = string.Empty;
                    foreach (var pathPartial in pathSplitted.Where(s1 => !string.IsNullOrWhiteSpace(s1)))
                    {
                        fileName = pathPartial;
                    }
                    s = s.Replace(myMatch.Value, fileName);
                    // Add your code here
                }
            }
            return string.Format("{0}", s);
        }

    }
}