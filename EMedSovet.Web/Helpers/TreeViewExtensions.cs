﻿using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using EMedSovet.Web.Models;

namespace EMedSovet.Web.Helpers
{

    public static class TreeViewExtensions
    {
        public static string Tree(this HtmlHelper helper, string name, Message message, int currentUserId, object htmlAttributes)
        {
            if (message == null) return string.Empty;

            var ul = new TagBuilder("ul");

            if (htmlAttributes != null)
                ul.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            ul.GenerateId(name);

            var liItems = new StringBuilder();

            //var li = new TagBuilder("li") {InnerHtml = helper.Encode(message.Text)};
            var li = new TagBuilder("li");

            var anchor = new TagBuilder("a") { InnerHtml = helper.Encode("ответить") };
            anchor.MergeAttributes(new RouteValueDictionary
                {
                    {"href", "#answerForm"},
                    {"class", "btn btn-mini btn-find fancybox answerButon"},
                    {"data-message-id", message.MessageId}
                });
            li.InnerHtml = message.IsLeaf && message.To.HasValue && message.To.Value == currentUserId
                               ? string.Format("{0} {1}", helper.Encode(message.Text),
                                               anchor.ToString(TagRenderMode.Normal))
                               : string.Format("{0}", helper.Encode(message.Text));
            
            liItems.AppendLine(li.ToString(TagRenderMode.Normal));
            foreach (var answer in message.MessagesAnswer)
            {
                liItems.AppendLine(Tree(helper, name, answer,currentUserId, htmlAttributes));
            }
            
            ul.InnerHtml = liItems.ToString();
            return ul.ToString(TagRenderMode.Normal);

        }
    }
}