﻿namespace EMedSovet.Web.Helpers
{
    public class OptionItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}