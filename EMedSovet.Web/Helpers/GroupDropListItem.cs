﻿using System.Collections.Generic;

namespace EMedSovet.Web.Helpers
{
    public class GroupDropListItem
    {
        public string Name { get; set; }
        public List<OptionItem> Items { get; set; }
    }
}