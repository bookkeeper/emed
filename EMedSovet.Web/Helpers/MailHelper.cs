﻿using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace EMedSovet.Web.Helpers
{
    public class MailHelper
    {

        public static void SendMail(string from, string to, string subject, string body)
        {
            try
            {
                using (var mailMessage = new MailMessage(from, to, subject, body))
                {
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Priority = MailPriority.Normal;
                    mailMessage.BodyEncoding = Encoding.UTF8;
                    // Add a carbon copy recipient.
                    //var copy = new MailAddress(ConfigurationManager.AppSettings["EmailCC"]);
                    //var bcc = new MailAddress(ConfigurationManager.AppSettings["EmailBCC"]);
                    //mailMessage.CC.Add(copy);
                    //mailMessage.Bcc.Add(bcc);

                    var mailClient = new SmtpClient
                    {
                        Host = ConfigurationManager.AppSettings["SmtpServer"]
                    };
                    //mailClient.UseDefaultCredentials = false;
                    //mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                    //mailClient.EnableSsl = true;
#if DEBUG
                    mailClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"],
                                                                   ConfigurationManager.AppSettings["EmailPassword"]);
#endif
                    //Send delivers the message to the mail server
                    mailClient.Send(mailMessage);

                }
            }
            catch (SmtpException ex)
            {
                System.Diagnostics.Debug.WriteLine("SmtpException has oCCured:" + ex.Message);
            }
        }

    }
}