﻿using System.Web.Mvc;

namespace EMedSovet.Web.Helpers
{
    public static class ControllerNameExtensions
    {
        public static string ControllerName(this HtmlHelper helper)
        {
            return helper.ViewContext.RouteData.GetRequiredString("controller");

        }
    }

}