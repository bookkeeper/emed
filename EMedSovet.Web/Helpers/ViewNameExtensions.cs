﻿using System.Web.Mvc;

namespace EMedSovet.Web.Helpers
{
    public static class ViewNameExtensions
    {
        public static string ViewName(this HtmlHelper helper)
        {
            return helper.ViewContext.RouteData.GetRequiredString("action");

        }
    }

}