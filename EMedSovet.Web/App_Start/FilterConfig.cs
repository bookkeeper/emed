﻿using System.Web;
using System.Web.Mvc;
using EMedSovet.Web.Filters;

namespace EMedSovet.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new InitializeSimpleMembershipAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}