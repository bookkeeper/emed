﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace EMedSovet.Web
{
    public class GlobalizationConfig
    {
        public static void RegisterCulture()
        {
            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
        }
    }
}
