﻿using System.Web.Optimization;
using EMedSovet.Web.Bundlers;

namespace EMedSovet.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js/bootstrap").Include(
                        "~/Scripts/bootstrap/js/bootstrap.js",
                        "~/Scripts/bootstrap/js/bootstrap-image-gallery.js",
                        "~/Scripts/jquery.mousewheel.js",
                        "~/Scripts/mwheelIntent.js",
                        "~/Scripts/jquery.jscrollpane.js"));

            bundles.Add(new ScriptBundle("~/bundles/jcarousel").Include(
                        "~/Scripts/jquery.jcarousel.js"));

            bundles.Add(new ScriptBundle("~/bundles/fancybox/js").Include(
                        "~/Scripts/fancybox/jquery.fancybox.js"));

            bundles.Add(new ScriptBundle("~/bundles/symptoms").Include(
                       "~/Scripts/symptoms.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(
                new StyleBundle("~/Content/bootstrap/css/bootstrap").Include(
                    "~/Content/bootstrap/css/bootstrap.css",
                    //"~/Content/bootstrap/css/bootstrap-responsive.css",
                    "~/Content/bootstrap/css/bootstrap-image-gallery.css",
                    "~/Content/jquery.jscrollpane.css"));

            bundles.Add(
                new StyleBundle("~/Content/jcarousel/skins/tango/jcarousel").Include(
                    "~/Content/jcarousel/skins/tango/skin.css"));

            bundles.Add(new StyleBundle("~/Scripts/fancybox/css").Include(
                        "~/Scripts/fancybox/jquery.fancybox.css"));

            bundles.Add(
                new StyleBundle("~/Content/FileUpload/fileupload").Include(
                    "~/Content/FileUpload/jquery.fileupload-ui.css"));

            bundles.Add(new ScriptBundle("~/bundles/fileupload").Include(
                        //"~/Content/FileUpload/jquery.tmpl.js",
                        "~/Content/FileUpload/canvas-to-blob.js",
                        "~/Content/FileUpload/load-image.js",
                        "~/Content/FileUpload/jquery.iframe-transport.js",
                        "~/Content/FileUpload/jquery.fileupload.js",
                        "~/Content/FileUpload/jquery.fileupload-ip.js",
                        "~/Content/FileUpload/jquery.fileupload-ui.js",
                        "~/Content/FileUpload/locale.js",
                        "~/Content/FileUpload/main.js"));

            bundles.Add(new ScriptBundle("~/bundles/jHtmlArea/js").Include(
                       "~/Content/jHtmlArea/jHtmlArea-0.7.5.js",
                       "~/Content/jHtmlArea/jHtmlArea.ColorPickerMenu-0.7.0.js"));

            bundles.Add(
                new StyleBundle("~/Content/jHtmlArea/style/css").Include(
                    "~/Content/jHtmlArea/style/jHtmlArea.css",
                    "~/Content/jHtmlArea/style/jHtmlArea.ColorPickerMenu.css"));
            bundles.Add(
                new StyleBundle("~/Content/css").Include(
                    "~/Content/style.css",
                    "~/Content/form.css",
                    "~/Content/news.css",
                    "~/Content/tabs.css",
                    "~/Content/articles.css",
                    "~/Content/mess_comments.css",
                    "~/Content/paging.css",
                    "~/Content/registration.css",
                    "~/Content/autocomplete.css",
                    "~/Content/vendor.css"));

            bundles.Add(new ScriptBundle("~/bundles/easyslider/js/js").Include(
                       "~/Scripts/easyslider/js/easySlider1.7.js"));

            bundles.Add(
               new StyleBundle("~/Scripts/easyslider/css/css").Include(
                   "~/Scripts/easyslider/css/screen.css"));

            bundles.Add(new ScriptBundle("~/bundles/Script/cookies/js").Include(
                       "~/Scripts/jquery.cookie.js",
                       "~/Scripts/jquery.json-2.3.js"));

            var coffeeBundleAndMinify = new CoffeeScriptTransform();
            var coffeeBundle = new Bundle("~/bundles/coffee", coffeeBundleAndMinify)
                                   .Include("~/Scripts/coffee/*.coffee");
            coffeeBundle.Transforms.Add(new JsMinify());
            BundleTable.Bundles.Add(coffeeBundle);

            var lessTransformAndMinify = new LessTransform();
            var lessBundle = new Bundle("~/bundles/less", lessTransformAndMinify).IncludeDirectory("~/Content/less",
                                                                                                   "*.less");
            lessBundle.Transforms.Add(new CssMinify());
            bundles.Add(lessBundle);

        }
    }
}