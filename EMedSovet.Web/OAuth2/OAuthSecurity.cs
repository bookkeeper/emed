﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;

namespace EMedSovet.Web.OAuth2
{
    public class OAuthSecurity
    {
        /// <summary>
        /// Registers the Microsoft account client.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientSecret">The client secret.</param>
        public static void RegisterMicrosoftClient(string clientId, string clientSecret)
        {
            RegisterMicrosoftClient(clientId, clientSecret, displayName: "Microsoft");
        }

        /// <summary>
        /// Registers the Microsoft account client.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientSecret">The client secret.</param>
        /// <param name="displayName">The display name.</param>
        public static void RegisterMicrosoftClient(string clientId, string clientSecret, string displayName)
        {
            RegisterMicrosoftClient(clientId, clientSecret, displayName, new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the Microsoft account client.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientSecret">The client secret.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about this client</param>
        public static void RegisterMicrosoftClient(string clientId, string clientSecret, string displayName, IDictionary<string, object> extraData)
        {
            RegisterClient(new MicrosoftClient(clientId, clientSecret), displayName, extraData);
        }

        /// Registers an authentication client.
        /// </summary>
        /// <param name="client">The client to be registered.</param>
        [CLSCompliant(false)]
        public static void RegisterClient(IAuthenticationClient client)
        {
            RegisterClient(client, displayName: null, extraData: new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers an authentication client.
        /// </summary>
        /// <param name="client">The client to be registered</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about the specified client</param>
        [CLSCompliant(false)]
        public static void RegisterClient(IAuthenticationClient client, string displayName, IDictionary<string, object> extraData)
        {
            if (client == null)
            {
                throw new ArgumentNullException("client");
            }

            if (String.IsNullOrEmpty(client.ProviderName))
            {
                throw new ArgumentException(WebResources.InvalidServiceProviderName, "client");
            }

            if (_authenticationClients.ContainsKey(client.ProviderName))
            {
                throw new ArgumentException(WebResources.ServiceProviderNameExists, "client");
            }

            var clientData = new AuthenticationClientData(client, displayName, extraData);
            _authenticationClients.Add(client.ProviderName, clientData);
        }
    }
}