﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EMedSovet.Web.OAuth2
{
    /// <summary>
    /// A grab-bag of utility methods useful for the channel stack of the protocol.
    /// </summary>
    [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Utility class touches lots of surface area")]
    public static class MessagingUtilities
    {
        /// <summary>
        /// The cryptographically strong random data generator used for creating secrets.
        /// </summary>
        /// <remarks>The random number generator is thread-safe.</remarks>
        internal static readonly RandomNumberGenerator CryptoRandomDataGenerator = new RNGCryptoServiceProvider();

        /// <summary>
        /// A pseudo-random data generator (NOT cryptographically strong random data)
        /// </summary>
        internal static readonly Random NonCryptoRandomDataGenerator = new Random();

        /// <summary>
        /// The uppercase alphabet.
        /// </summary>
        internal const string UppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /// <summary>
        /// The lowercase alphabet.
        /// </summary>
        internal const string LowercaseLetters = "abcdefghijklmnopqrstuvwxyz";

        /// <summary>
        /// The set of base 10 digits.
        /// </summary>
        internal const string Digits = "0123456789";

        /// <summary>
        /// The set of digits and alphabetic letters (upper and lowercase).
        /// </summary>
        internal const string AlphaNumeric = UppercaseLetters + LowercaseLetters + Digits;

        /// <summary>
        /// All the characters that are allowed for use as a base64 encoding character.
        /// </summary>
        internal const string Base64Characters = AlphaNumeric + "+" + "/";

        /// <summary>
        /// All the characters that are allowed for use as a base64 encoding character
        /// in the "web safe" context.
        /// </summary>
        internal const string Base64WebSafeCharacters = AlphaNumeric + "-" + "_";

        /// <summary>
        /// The set of digits, and alphabetic letters (upper and lowercase) that are clearly
        /// visually distinguishable.
        /// </summary>
        internal const string AlphaNumericNoLookAlikes = "23456789abcdefghjkmnpqrstwxyzABCDEFGHJKMNPQRSTWXYZ";

        /// <summary>
        /// The length of private symmetric secret handles.
        /// </summary>
        /// <remarks>
        /// This value needn't be high, as we only expect to have a small handful of unexpired secrets at a time,
        /// and handle recycling is permissible.
        /// </remarks>
        private const int SymmetricSecretHandleLength = 4;


        /// <summary>
        /// A character array containing just the = character.
        /// </summary>
        private static readonly char[] EqualsArray = new char[] { '=' };

        /// <summary>
        /// A character array containing just the , character.
        /// </summary>
        private static readonly char[] CommaArray = new char[] { ',' };

        /// <summary>
        /// A character array containing just the " character.
        /// </summary>
        private static readonly char[] QuoteArray = new char[] { '"' };

        /// <summary>
        /// The set of characters that are unreserved in RFC 2396 but are NOT unreserved in RFC 3986.
        /// </summary>
        private static readonly string[] UriRfc3986CharsToEscape = new[] { "!", "*", "'", "(", ")" };

        /// <summary>
        /// A set of escaping mappings that help secure a string from javscript execution.
        /// </summary>
        /// <remarks>
        /// The characters to escape here are inspired by 
        /// http://code.google.com/p/doctype/wiki/ArticleXSSInJavaScript
        /// </remarks>
        private static readonly Dictionary<string, string> javascriptStaticStringEscaping = new Dictionary<string, string> {
			{ "\\", @"\\" }, // this WAS just above the & substitution but we moved it here to prevent double-escaping
			{ "\t", @"\t" },
			{ "\n", @"\n" },
			{ "\r", @"\r" },
			{ "\u0085", @"\u0085" },
			{ "\u2028", @"\u2028" },
			{ "\u2029", @"\u2029" },
			{ "'", @"\x27" },
			{ "\"", @"\x22" },
			{ "&", @"\x26" },
			{ "<", @"\x3c" },
			{ ">", @"\x3e" },
			{ "=", @"\x3d" },
		};

        /// <summary>
        /// The available compression algorithms.
        /// </summary>
        internal enum CompressionMethod
        {
            /// <summary>
            /// The Deflate algorithm.
            /// </summary>
            Deflate,

            /// <summary>
            /// The GZip algorithm.
            /// </summary>
            Gzip,
        }

        /// <summary>
        /// Concatenates a list of name-value pairs as key=value&amp;key=value,
        /// taking care to properly encode each key and value for URL
        /// transmission according to RFC 3986.  No ? is prefixed to the string.
        /// </summary>
        /// <param name="args">The dictionary of key/values to read from.</param>
        /// <returns>The formulated querystring style string.</returns>
        internal static string CreateQueryString(IEnumerable<KeyValuePair<string, string>> args)
        {
            //Requires.NotNull(args, "args");
            Contract.Ensures(Contract.Result<string>() != null);

            if (!args.Any())
            {
                return string.Empty;
            }
            StringBuilder sb = new StringBuilder(args.Count() * 10);

            foreach (var p in args)
            {
                //ErrorUtilities.VerifyArgument(!string.IsNullOrEmpty(p.Key), MessagingStrings.UnexpectedNullOrEmptyKey);
                //ErrorUtilities.VerifyArgument(p.Value != null, MessagingStrings.UnexpectedNullValue, p.Key);
                sb.Append(EscapeUriDataStringRfc3986(p.Key));
                sb.Append('=');
                sb.Append(EscapeUriDataStringRfc3986(p.Value));
                sb.Append('&');
            }
            sb.Length--; // remove trailing &

            return sb.ToString();
        }

        /// <summary>
        /// Escapes a string according to the URI data string rules given in RFC 3986.
        /// </summary>
        /// <param name="value">The value to escape.</param>
        /// <returns>The escaped value.</returns>
        /// <remarks>
        /// The <see cref="Uri.EscapeDataString"/> method is <i>supposed</i> to take on
        /// RFC 3986 behavior if certain elements are present in a .config file.  Even if this
        /// actually worked (which in my experiments it <i>doesn't</i>), we can't rely on every
        /// host actually having this configuration element present.
        /// </remarks>
        internal static string EscapeUriDataStringRfc3986(string value)
        {
            //Requires.NotNull(value, "value");

            // Start with RFC 2396 escaping by calling the .NET method to do the work.
            // This MAY sometimes exhibit RFC 3986 behavior (according to the documentation).
            // If it does, the escaping we do that follows it will be a no-op since the
            // characters we search for to replace can't possibly exist in the string.
            StringBuilder escaped = new StringBuilder(Uri.EscapeDataString(value));

            // Upgrade the escaping to RFC 3986, if necessary.
            for (int i = 0; i < UriRfc3986CharsToEscape.Length; i++)
            {
                escaped.Replace(UriRfc3986CharsToEscape[i], Uri.HexEscape(UriRfc3986CharsToEscape[i][0]));
            }

            // Return the fully-RFC3986-escaped string.
            return escaped.ToString();
        }







        /// <summary>
        /// Adds a set of name-value pairs to the end of a given URL
        /// as part of the querystring piece.  Prefixes a ? or &amp; before
        /// first element as necessary.
        /// </summary>
        /// <param name="builder">The UriBuilder to add arguments to.</param>
        /// <param name="args">
        /// The arguments to add to the query.  
        /// If null, <paramref name="builder"/> is not changed.
        /// </param>
        /// <remarks>
        /// If the parameters to add match names of parameters that already are defined
        /// in the query string, the existing ones are <i>not</i> replaced.
        /// </remarks>
        internal static void AppendQueryArgs(this UriBuilder builder, IEnumerable<KeyValuePair<string, string>> args)
        {
            //Requires.NotNull(builder, "builder");

            if (args != null && args.Count() > 0)
            {
                StringBuilder sb = new StringBuilder(50 + (args.Count() * 10));
                if (!string.IsNullOrEmpty(builder.Query))
                {
                    sb.Append(builder.Query.Substring(1));
                    sb.Append('&');
                }
                sb.Append(CreateQueryString(args));

                builder.Query = sb.ToString();
            }
        }



        
        
    }
}