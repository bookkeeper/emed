﻿using System;
using System.Text;
using System.Web;
using System.Web.Optimization;
using dotless.Core;

namespace EMedSovet.Web.Bundlers
{
    public class LessTransform : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse response)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (response == null)
                throw new ArgumentNullException("response");

            var content = new StringBuilder();
            foreach (var fileInfo in response.Files)
            {
                using (var reader = fileInfo.OpenText())
                {
                    content.Append(reader.ReadToEnd());
                }
            }
            var cssCompiled = Less.Parse(content.ToString());
            response.Content = cssCompiled;
            response.ContentType = "text/css";
            response.Cacheability = HttpCacheability.Public;
        }

    }
}