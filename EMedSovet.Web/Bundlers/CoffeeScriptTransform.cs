﻿using System.Text;
using System.Web.Optimization;

namespace EMedSovet.Web.Bundlers
{
    public class CoffeeScriptTransform : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse response)
        {
            var compiler = new CoffeeSharp.CoffeeScriptEngine();
            var content = new StringBuilder();
            foreach (var fileInfo in response.Files)
            {
                using (var reader = fileInfo.OpenText())
                {
                    content.Append(reader.ReadToEnd());
                }
            }

            var jsCompiled = compiler.Compile(content.ToString());
            context.HttpContext.Response.Cache.SetLastModifiedFromFileDependencies();
            response.Content = jsCompiled;
            response.ContentType = "text/javascript";
        }

    }
}