﻿using System.Collections.Generic;

namespace DiseaseParser
{
    public class LinkItem
    {
        public string Href;
        public string Text;
        public string Article;
        public List<string> Pages = new List<string>();

        public override string ToString()
        {
            return Href + "\n\t" + Text;
        }
    }
}
