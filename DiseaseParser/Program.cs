﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiseaseParser
{
    class Program
    {
        public const string HOST = "http://www.zdr.ru";
        public static string[] PageLetters = new[] { "a", "b", "v", "g", "d", "j", "z", "i", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "shh", "ee", "ia" };
        public static int[] PageNumbers = new[] { 1, 2 };
        public static List<LinkItem> ArticleLinksList = new List<LinkItem>();

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            try
            {
                var iterator = 0;
                foreach (var pageLetter in PageLetters)
                {
                    foreach (var pageNumber in PageNumbers)
                    {
                        var html = GetPageData(pageLetter, pageNumber);

                        var h3Text = GetH3Text(html);
                        //Console.WriteLine(h3Text);
                        GetArticleLinks(h3Text, ArticleLinksList);
                    }
                    iterator++;
                    //if (iterator == 1) break;
                }

                foreach (var articleLink in ArticleLinksList)
                {
                    Console.WriteLine(articleLink.Href);
                    var articleText = GetArticle(articleLink);
                    SaveArticleFile(articleText, articleLink);
                    //break;
                }
            }
            finally
            {
                Console.WriteLine("[Done]");
                Console.ReadLine();
            }

        }

        private static void SaveArticleFile(string articleText, LinkItem articleLink)
        {
            var docPath = string.Format("{0}\\Articles", Directory.GetCurrentDirectory());
            if (!Directory.Exists(docPath)) Directory.CreateDirectory(docPath);

            var name = Regex.Replace(articleLink.Text, " ", "_");
            name = Regex.Replace(name, ":", "_");
            var filename = string.Format("{0}\\{1}.html", docPath, name);
            
            if (File.Exists(filename)) return;
            var sbArticleText = new StringBuilder(GetPreHtml());
            sbArticleText.AppendFormat("{0}", articleText);
            sbArticleText.AppendFormat("{0}", GetPostHtml());
            WriteCSharpCodeInsert(articleLink);
            using (var outfile = new StreamWriter(filename,true, Encoding.GetEncoding(1251)))
            {
                outfile.Write(sbArticleText.ToString());
            }

        }

        private static string GetPostHtml()
        {
            return @"</body>
                    </html>";
        }

        private static string GetPreHtml()
        {
            return @"<!DOCTYPE html>
                    <html>
                    <head>
                    <meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1251"" />
                    </head>
                    <body>";
        }

        private static void WriteCSharpCodeInsert(LinkItem articleLink)
        {
            var docPath = string.Format("{0}\\Articles", Directory.GetCurrentDirectory());
            if (!Directory.Exists(docPath)) Directory.CreateDirectory(docPath);
            var filenameCs = string.Format("{0}\\CSharpCodeInsert.cs", docPath);
            var name = Regex.Replace(articleLink.Text, " ", "_");
            name = Regex.Replace(name, ":", "_");
            var articleFilename = string.Format("{0}.html", name);
            //if (!File.Exists(filename)) File.Create(filename).Close();
            var cs = string.Format("new Disease {{Name=\"{0}\", Description = string.Empty, Article = \"{1}\"}}, ",
                                   articleLink.Text, articleFilename);
            using (var sw = File.AppendText(filenameCs))
            {
                sw.WriteLine(cs);
            }
        }

        private static string GetArticle(LinkItem articleLink)
        {
            var html = string.Empty;
            var url = string.Format("{0}{1}", HOST, articleLink.Href);
            Console.WriteLine("Downloading: {0}", url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "Get";
            var response = (HttpWebResponse)request.GetResponse();
            //Console.WriteLine(response.StatusDescription);
            var dataStream = response.GetResponseStream();
            if (dataStream != null)
            {
                var reader = new StreamReader(dataStream, Encoding.GetEncoding(1251));
                var responseFromServer = reader.ReadToEnd();
                html = response.ResponseUri.AbsoluteUri == "http://www.zdr.ru/encyclopaedia/index.html"
                           ? string.Empty
                           : responseFromServer;

                //Console.WriteLine(responseFromServer);
                reader.Close();
                dataStream.Close();
            }
            response.Close();

            var articleText = GetArticleText(html);
            Console.WriteLine(articleText);
            return articleText;

        }

        private static string GetArticleText(string html)
        {
            const string pattern = @"(<div[^>]*?class=""text""[^>]*?>(.*?)</div>)";
            //var tag = "h3";
            //var pattern = string.Format(@"<{0}[^<>]*class=""info""[^<>]*>(?<tegData>.+?)</{0}>", tag.Trim());
            //const string pattern = @"<div[^>]*?class=""info""[^>]*?>(.*?)</div>";

            var matchLinksContainer = Regex.Matches(html, pattern,
                RegexOptions.Singleline);

            // Loop over each match.
            var divText = new StringBuilder();
            var iterator = 0;
            foreach (Match match in matchLinksContainer)
            {
                if (iterator == 0)
                    divText.AppendFormat("{0}", match.Groups[1].Value);
                iterator++;

            }
            return divText.ToString();

        }

        private static void GetArticleLinks(string h3Text, List<LinkItem> articleLinksList)
        {
            // 1.
            // Find all matches in file.
            var matchLinks = Regex.Matches(h3Text, @"(<a.*?>.*?</a>)",
                RegexOptions.Singleline);

            // 2.
            // Loop over each match.
            foreach (Match match in matchLinks)
            {
                var value = match.Groups[1].Value;
                var linkItem = new LinkItem();

                // 3.
                // Get href attribute.
                var matchHref = Regex.Match(value, @"href=\""(.*?)\""",
                RegexOptions.Singleline);
                if (matchHref.Success)
                {
                    linkItem.Href = matchHref.Groups[1].Value;
                }

                // 4.
                // Remove inner tags from text.
                var text = Regex.Replace(value, @"\s*<.*?>\s*", string.Empty,
                RegexOptions.Singleline);
                linkItem.Text = text;

                articleLinksList.Add(linkItem);
            }
        }

        private static string GetH3Text(string html)
        {
            const string pattern = @"(<h3.*?>.*?</h3>)";
            //const string pattern = @"(<h3.*?>.*?<br>)";
            //var tag = "h3";
            //var pattern = string.Format(@"<{0}[^<>]*class=""info""[^<>]*>(?<tegData>.+?)</{0}>", tag.Trim());
            //const string pattern = @"<div[^>]*?class=""info""[^>]*?>(.*?)</div>";

            var matchLinksContainer = Regex.Matches(html, pattern,
                RegexOptions.Singleline);

            // Loop over each match.
            var h3Text = new StringBuilder();
            foreach (Match match in matchLinksContainer)
            {
                if (match.Groups[1].Value.ToLower().Contains("full-article"))
                    h3Text.AppendFormat("{0}", match.Groups[1].Value);
            }
            return h3Text.ToString();
        }

        private static string GetPageData(string pageLetter, int pageNumber)
        {
            var html = string.Empty;
            var url = string.Format("{0}/encyclopaedia/entsiklopedija-boleznej/{1}/page-{2}.html", HOST, pageLetter,
                                    pageNumber);
            Console.WriteLine("Downloading: {0}", url);
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "Get";
            var response = (HttpWebResponse)request.GetResponse();
            //Console.WriteLine(response.StatusDescription);
            var dataStream = response.GetResponseStream();
            if (dataStream != null)
            {
                var reader = new StreamReader(dataStream, Encoding.GetEncoding(1251));
                var responseFromServer = reader.ReadToEnd();
                html = response.ResponseUri.AbsoluteUri == "http://www.zdr.ru/encyclopaedia/index.html"
                           ? string.Empty
                           : responseFromServer;

                //Console.WriteLine(responseFromServer);
                reader.Close();
                dataStream.Close();
            }
            response.Close();

            return html;
            //string html; 
            //using (var client = new WebClient())
            //{
            //    SetWebClientHeader(client);
            //    var url = string.Format("{0}/encyclopaedia/entsiklopedija-boleznej/{1}/page-{2}.html", HOST, pageLetter,
            //                            pageNumber);
            //    var data = client.DownloadData(url);
            //    Console.WriteLine(client.Headers["Status Code"]);
            //    //var decompress = Decompress(data);
            //    html = Encoding.GetEncoding(1251).GetString(data);
            //    //Console.WriteLine("Size from network: {0}", data.Length);
            //    //Console.WriteLine("Size decompressed: {0}", decompress.Length);
            //    //Console.WriteLine("Page:       {0}", html);

            //}
            //return html;
        }

        private static void SetWebClientHeader(WebClient client)
        {
            client.Headers["User-Agent"] =
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11";
            //client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
        }

        static byte[] Decompress(byte[] gzip)
        {
            using (var stream = new GZipStream(new MemoryStream(gzip),
                                  CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (var memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }


    }


    static class LinkFinder
    {
        public static List<LinkItem> Find(string html)
        {

            var matchLinksContainer = Regex.Matches(html, @"(<div class=""links"".*?>.*?</div>)",
                RegexOptions.Singleline);

            // Loop over each match.
            var divLinks = string.Empty;
            foreach (Match match in matchLinksContainer)
            {
                divLinks = match.Groups[1].Value;
                break;
            }
            //Console.WriteLine(divLinks);
            var linkItemList = new List<LinkItem>();



            // 1.
            // Find all matches in file.
            var matchLinks = Regex.Matches(divLinks, @"(<a.*?>.*?</a>)",
                RegexOptions.Singleline);

            // 2.
            // Loop over each match.
            foreach (Match match in matchLinks)
            {
                string value = match.Groups[1].Value;
                var linkItem = new LinkItem();

                // 3.
                // Get href attribute.
                var matchHref = Regex.Match(value, @"href=\""(.*?)\""",
                RegexOptions.Singleline);
                if (matchHref.Success)
                {
                    linkItem.Href = matchHref.Groups[1].Value;
                }

                // 4.
                // Remove inner tags from text.
                var text = Regex.Replace(value, @"\s*<.*?>\s*", "",
                RegexOptions.Singleline);
                linkItem.Text = text;

                linkItemList.Add(linkItem);
            }
            return linkItemList;
        }
    }


}
